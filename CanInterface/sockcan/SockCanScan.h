/*
 * SockCanScan.h
 *
 *  Created on: Jul 21, 2011
 *      Author: vfilimon
 *      Rework by pnikiel
 */

#ifndef SOCKCANSCAN_H_
#define SOCKCANSCAN_H_

#include <pthread.h>
#include <unistd.h>
#ifdef WIN32
#include "Winsock2.h"
#endif

#include <string>
using namespace std;
#include "CCanAccess.h"

#include "CanStatistics.h"

#include <sys/socket.h>
#include <linux/can.h>
#include <linux/sockios.h>

class CSockCanScan : public CCanAccess
{
 public:
  
  CSockCanScan();
  CSockCanScan(CSockCanScan const & other) = delete;
  CSockCanScan& operator=( CSockCanScan const & other) = delete;
    
    virtual ~CSockCanScan();
    
    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message, bool rtr);
    virtual bool sendRemoteRequest(short cobID);
    virtual bool createBUS(const char *name ,const  char *parameters );
    
    string &getNamePort() { return name_of_port; }
    int getHandler() { return m_sock; }
    string &getChannel() { return channelName; }
    
    void getStatistics( CanStatistics & result );


    bool stopBus ();
    void updateInitialError () ;

    static std::string errorFrameToString (const can_frame &f);


 private:

    volatile bool m_runCan;
    int m_sock;
    
    CanStatistics m_statistics;

    pthread_t   m_hCanScanThread;
    int         m_idCanScanThread;

    void sendErrorMessage(const char  *);
    void sendErrorMessage(const struct can_frame *);

    void clearErrorMessage();

    int configureCanboard(const char *,const char *);
    int openCanPort();

    //TODO what is this
    void waitCanDriver();
    static void* CanScanControlThread(void *);

    // TODO failed naming convention
    string name_of_port;

    // TODO failed naming convention
    string channelName;

    // TODO failed naming convention
    string param;

    //! Current baud rate
    unsigned int m_baudRate;

    //! It means that the application should only use the port, but not reconfigure (e.g. change bitrate, turn port on/off, etc)
    bool m_dontReconfigure;

    //! As up-to-date as possible state of the interface.
    int m_errorCode;
};


#endif /* SOCKCANSCAN_H_ */
