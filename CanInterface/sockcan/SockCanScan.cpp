/*
 * SockCanScan.cpp
 *
 *  Created on: Jul 21, 2011
 *
 *  Based on work by vfilimon
 *  Rework and logging done by Piotr Nikiel <piotr@nikiel.info>
 *
 */
#include "SockCanScan.h"
#include <errno.h>

#include <fstream>
#include <iostream>
#include <strstream>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>

#ifndef peak_can
#include <libsocketcan.h>
#endif

#include <CanMessage.h>
#include <LogIt.h>
#include <LogItComponentIds.h>
#include <Utils.h>
#include <WienerServerUtils.h>

//! The macro below is applicable only to this translation unit
#define MLOG(LEVEL,THIS) LOG(Log::LEVEL, LOG_CAN_INTERFACE) << THIS->channelName << " "

extern "C" CCanAccess *getCanbusAccess()
{

  LOG(Log::TRC, LOG_CAN_INTERFACE) << "in getCanbusAccess ";
  CCanAccess *cc;
  cc = new CSockCanScan();
  return cc;
}

CSockCanScan::CSockCanScan() :
				m_sock(0),
				m_baudRate(0),
				m_dontReconfigure (false),
				m_errorCode(-1)
{
	m_statistics.beginNewRun();

}

static std::string canFrameToString(const can_frame &f)
{
	std::string result;
	result =  "[id=0x"+toHexString(f.can_id, 3, '0')+" ";
	if (f.can_id & CAN_RTR_FLAG)
		result += "RTR ";
	result+="dlc=" + Utils::toString(int(f.can_dlc)) + " data=[";

	for (int i=0; i<f.can_dlc; i++)
		result+= toHexString((unsigned int)f.data[i], 2, '0')+" ";
	result += "]]";

	return result;
}

void* CSockCanScan::CanScanControlThread(void *pCanScan)
{
	CanMessage cmsg;
	int timeErr;
	struct can_frame  sockmsg;
	CSockCanScan *ppcs = static_cast<CSockCanScan *>(pCanScan);
	ppcs->m_runCan = true;
	int sock = ppcs->m_sock;
	int nbytes;


	while (ppcs->m_runCan)
	{
		/* Assumption: at this moment sock holds meaningful value. */

		fd_set set;
		FD_ZERO( &set );
		FD_SET( sock, &set );
		timeval timeout;
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;
		int selectResult = select( sock+1, &set, 0, &set, &timeout );

		if (selectResult<0)
		{
			MLOG(ERR,ppcs) << "select() failed: " << errnoToString();
			usleep (1000000);
			continue;

		}
		/* Now select result >=0 so it was either nothing received (timeout) or something received */

		if (ppcs->m_errorCode)
		{
			/* The preceeding call took either 'timeout' time, or there is frame received -- perfect time to attempt to clean error frame. */
			int newErrorCode = -1;
			int ret = can_get_state( ppcs->channelName.c_str(), &newErrorCode);
			if (ret != 0)
			{
				MLOG(ERR,ppcs) << "can_get_state failed.";
			}
			else
			{
				if (newErrorCode == 0)
				{
					ppcs->m_errorCode = 0;
					timeval t;
					gettimeofday( &t, 0 );
					ppcs->canMessageError( ppcs->m_errorCode, "CAN port is recovered", t );
				}
			}
		}


		if (selectResult > 0)
		{
			nbytes = read(sock, &sockmsg, sizeof(struct can_frame));
			MLOG(DBG,ppcs) << "read(): " << canFrameToString(sockmsg);
			if (nbytes < 0)
			{
				const int seconds = 10;
				MLOG(ERR,ppcs) << "read() error: " << errnoToString();
				timeval now;
				gettimeofday( &now, 0 );
				ppcs->canMessageError( nbytes, ("read() error: "+errnoToString()).c_str(), now );
				ppcs->m_errorCode = -1;
				do
				{
					MLOG(INF,ppcs) << "Waiting " << seconds << " seconds.";
					sleep(seconds);
					if (sock>0)
					{
						MLOG(INF,ppcs) << "Closing socket";
						if (close(sock) < 0)
						{
							MLOG(ERR,ppcs) << "Socket close error!";
						}
						else
						{
							MLOG(INF,ppcs) << "Socket closed";
							sock = -1;
						}
					}
					MLOG(INF,ppcs) << "Now port will be reopened.";
					if ((sock = ppcs->openCanPort()) < 0)
					{
						MLOG(ERR,ppcs) << "openCanPort() failed.";
					}
					else
						MLOG(INF,ppcs) << "Port reopened.";
				}
				while (ppcs->m_runCan && sock<0);
				continue;
			}

			if (nbytes <(int) sizeof(struct can_frame))
			{
				LOG(Log::INF) << ppcs->channelName.c_str() << " incomplete frame received, nbytes=" << nbytes;
				continue;
			}
			if (sockmsg.can_id & CAN_ERR_FLAG)
			{

				/* With this mechanism we only set the portError */
				ppcs->m_errorCode = sockmsg.can_id & ~CAN_ERR_FLAG;
				std::string description = errorFrameToString( sockmsg );
				MLOG(ERR,ppcs) << "SocketCAN error frame: " << description << " original: " << canFrameToString(sockmsg) ;
				timeval c_time;
				timeErr = ioctl(ppcs->m_sock, SIOCGSTAMP, &c_time);
				ppcs->canMessageError( ppcs->m_errorCode, description.c_str(), c_time );

				continue;
			}



			cmsg.c_rtr = sockmsg.can_id & CAN_RTR_FLAG;
			// this actually should exclude more bits
			cmsg.c_id = sockmsg.can_id & ~CAN_RTR_FLAG;
			timeErr = ioctl(sock,SIOCGSTAMP,&cmsg.c_time);
			cmsg.c_dlc = sockmsg.can_dlc;
			memcpy(&cmsg.c_data[0],&sockmsg.data[0],8);
			ppcs->canMessageCame(cmsg);
			ppcs->m_statistics.onReceive( sockmsg.can_dlc );
		}
	}

	MLOG(INF,ppcs) << "Main loop of SockCanScan terminated." ;
	
	// loop finished, close open socket
	// These is a redundent code
	if (sock) {
		printf ("Closing socket for %s ...\n", ppcs->channelName.c_str());
		if (close(sock) < 0) {
			perror("Socket close error!");
		}
		else printf ("Socket for %s closed.\n", ppcs->channelName.c_str());
		sock = 0;
	}

	pthread_exit(NULL);
	return NULL;
}

CSockCanScan::~CSockCanScan()
{
	stopBus();
}

int CSockCanScan::configureCanboard(const char *name,const char *parameters)
{
  const char		*com;		// chennal name
  int		delta;
  com = strchr(name,':');
  if (com == NULL) {

    com = name;
  }
  else {
    delta = com - name;
    com++;
  }

  channelName = com;
  param = parameters;
  return openCanPort();
}

/** Obtains a SocketCAN socket and opens it.
 *  The name of the port and parameters should have been specified by preceding call to configureCanboard()
 *
 *  @returns less than zero in case of error, otherwise success
 */
int CSockCanScan::openCanPort()
{

  bool	paramSpecified = true;
  struct ifreq ifr;

  int err;
  unsigned int br;
  
  /* Determine if it was requested through params to configure the port (baudrate given) or just to open it ("Unspecified")*/
  if (param.length() > 0) {
	if (param != "Unspecified")
	{
    	int numPar = sscanf( param.c_str(), "%d ", &br );
    	if (numPar < 1)
    	{
    		MLOG(ERR,this) << "While parsing parameters: this syntax is incorrect: '" << param << "'";
    		return -1;
    	}
	}
	else
	{
		paramSpecified = false;
		m_dontReconfigure = true;
	}
  }

  /* If requested to configure */
  if (paramSpecified)
  {
  	err = can_do_stop(channelName.c_str());
  	if (err < 0)
  	{
  		MLOG(ERR,this) << "While can_do_stop(): " << errnoToString();
    	return -1;
  	}
  	err = can_set_bitrate(channelName.c_str(),br);
  	if (err < 0)
  	{
    	MLOG(ERR,this) << "While can_set_bitrate(): " << errnoToString();
    	return -1;
  	}
  	m_baudRate = br;
  	err = can_do_start(channelName.c_str());
  	if (err < 0)
  	{
  		MLOG(ERR,this) << "While can_do_start(): " << errnoToString();
    	return -1;
  	}
  }

  m_sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
  if (m_sock < 0)
  {
	MLOG(ERR,this) << "While socket(): " << errnoToString();
    return -1;
  }
  memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
  if (channelName.length() > sizeof ifr.ifr_name-1)
  {
	  MLOG(ERR,this) << "Given name of the port exceeds operating-system imposed limit";
	  return -1;
  }
  strncpy(ifr.ifr_name, channelName.c_str(), sizeof(ifr.ifr_name)-1);

  if (ioctl(m_sock, SIOCGIFINDEX, &ifr) < 0)
  {
	MLOG(ERR,this) << "ioctl SIOCGIFINDEX failed. " << errnoToString();
    return -1;
  }

  can_err_mask_t err_mask = 0x1ff;
  if( setsockopt(m_sock, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof err_mask) < 0 )
  {
	  MLOG(ERR,this) << "setsockopt() failed: " << errnoToString();
	  return -1;
  }

  struct sockaddr_can addr;
  addr.can_family = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;

  if (::bind(m_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
  {
	MLOG(ERR,this) << "While bind(): " << errnoToString();
    return -1;
  }

  // Fetch initial state of the port
  if (int ret=can_get_state( channelName.c_str(), &m_errorCode ))
  {
	  m_errorCode = ret;
	  MLOG(ERR,this) << "can_get_state() failed with error code " << ret << ", it was not possible to obtain initial port state";
  }

  updateInitialError();

  return m_sock;
}


bool CSockCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message, bool rtr)
{
  struct can_frame frame;
  ssize_t nbytes;
  unsigned char *buf = message;
  frame.can_id = cobID;
  frame.can_dlc = len;
  int l, l1;
  l = len;

  if (rtr)
	frame.can_id |= CAN_RTR_FLAG;
  


  do {
    if (l > 8) {
      l1 = 8; l = l - 8;
    }
    else l1 = l;
    frame.can_dlc = l1;
    memcpy(frame.data,buf,l1);

    nbytes = write(m_sock, &frame, sizeof(struct can_frame));

    MLOG(DBG,this) << "write(): " << canFrameToString(frame) << " result=" << nbytes;

    if (nbytes < 0) /* ERROR */
    {
    	MLOG(ERR,this) << "While write() :" << errnoToString();
        if (errno == ENOBUFS) 
	    {	
		    printf ("ENOBUFS; waiting a jiffy ...\n");
	    	usleep(100000);
            continue;
         }
    }

    if (nbytes > 0)

    	m_statistics.onTransmit( frame.can_dlc );

    if (nbytes < (int)sizeof(struct can_frame)) {
	return false; 
    }
    buf = buf + l1;
  }
  while (l > 8);



  return true; 
}

bool CSockCanScan::sendRemoteRequest(short cobID)
{
  struct can_frame frame;
  int nbytes;
  frame.can_id = cobID + CAN_RTR_FLAG	;
  frame.can_dlc = 0;
  do {
    nbytes = write(m_sock, &frame, sizeof(struct can_frame));
    if (nbytes < 0) 	
    {
       perror("while write() on socket");
       if (errno == ENOBUFS) 
       {	
		  	printf ("ENOBUFS; waiting a jiffy ...\n");
		    // insert deleted systec buffer
		  	usleep(100000);
    	    continue;
		}
    }
    else {
      if (nbytes < (int)sizeof(struct can_frame))
      {
        printf ("Warning: sendRemoteRequest() sent less bytes than expected\n");
        return false;
      }
    }
    break;
  }
  while(true);

  return true; 
}

bool CSockCanScan::createBUS(const char *name ,const char *parameters)
{
  m_runCan = true;
  name_of_port = string(name);
  
  LOG(Log::INF, LOG_CAN_INTERFACE) << "Create bus " << name << " (parameters='" << parameters << "')";
  if ((m_sock = configureCanboard(name,parameters)) < 0) {
    return false;
  }
  LOG(Log::INF, LOG_CAN_INTERFACE) << "Starting main loop for CAN bus '" << name << '"';
  m_idCanScanThread =
    pthread_create(&m_hCanScanThread,NULL,&CanScanControlThread,
		   (void *)this);

  return (!m_idCanScanThread);
}

/** Provides textual representation of SocketCAN error.
 *
 */
std::string CSockCanScan::errorFrameToString(const can_frame &frame)
{
	std::string result;
	// Code below is taken from SysTec documentation
	if (frame.can_id & CAN_ERR_FLAG)
	{
		if (frame.can_id & CAN_ERR_TX_TIMEOUT)
			result += "TX_timeout ";
		if (frame.can_id & CAN_ERR_LOSTARB)
			result += "Arbitration_lost ";
		if (frame.can_id & CAN_ERR_CRTL)
		{
			result += "Controller_state=( ";
			if (frame.data[1] & CAN_ERR_CRTL_RX_WARNING)
				result += "RX_warning ";
			if (frame.data[1] & CAN_ERR_CRTL_TX_WARNING)
				result += "TX_warning ";
			if (frame.data[1] & CAN_ERR_CRTL_RX_PASSIVE)
				result += "RX_passive ";
			if (frame.data[1] & CAN_ERR_CRTL_TX_PASSIVE)
				result += "TX_passive ";
			result += ") ";
		}
		if (frame.can_id & CAN_ERR_PROT)
		{
			result += "Protocol_violation=( type=" + Utils::toHexString(frame.data[2]) ;
			if (frame.data[2] & CAN_ERR_PROT_ACTIVE )
				result += " Error_active(recovered) ";
			result += ") ";
			// TODO we could print in more details what sort of violation is that
		}
		if (frame.can_id & CAN_ERR_TRX)
		{
			result += "Transceiver status=( " + Utils::toHexString(frame.data[4]) + ") ";
			// TODO we could print in more details what sort of TRX status that is
		}
		if (frame.can_id & CAN_ERR_ACK)
		{
			result += "No_ACK ";
		}
		if (frame.can_id & CAN_ERR_BUSOFF)
		{
			result += "Bus_off ";
		}
		if (frame.can_id & CAN_ERR_BUSERROR)
		{
			result += "Bus_error ";
		}
		if (frame.can_id & CAN_ERR_RESTARTED)
		{
			result += "Controller_restarted ";
		}
	}
	else
		result = "No error";
	return result;
}

void CSockCanScan::clearErrorMessage()
{
  string errorMessage = "";
  int timeErr;
  timeval	c_time;
	
  timeErr = ioctl(m_sock,SIOCGSTAMP,&c_time);

  canMessageError(0,errorMessage.c_str(),c_time);
  return;
}

void CSockCanScan::sendErrorMessage(const char *mess)
{
  int timeErr;
  timeval	c_time;

  timeErr = ioctl(m_sock,SIOCGSTAMP,&c_time);

  canMessageError(-1,mess,c_time);
  return;
}

bool CSockCanScan::stopBus ()
{
	// notify the thread that it should finish.
	m_runCan = false;
	pthread_join( m_hCanScanThread, 0 );
	MLOG(INF,this) << "stopBus() finished";
	return true;
}

void CSockCanScan::getStatistics( CanStatistics & result )
{
	m_statistics.computeDerived (m_baudRate);
	result = m_statistics;  // copy whole structure
	m_statistics.beginNewRun();
}

void CSockCanScan::updateInitialError ()
{
	if (m_errorCode == 0)
		clearErrorMessage();
	else
	{
		timeval now;
		gettimeofday( &now, 0);
		canMessageError( m_errorCode, "Initial port state: error", now );
	}

}



