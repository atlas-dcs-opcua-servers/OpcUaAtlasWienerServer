project(OpcUaAtlasWienerServer HOMEPAGE_URL "https://gitlab.cern.ch/atlas-dcs-opcua-servers/OpcUaAtlasWienerServer")
cmake_minimum_required(VERSION 3.10)

# Get type of Linux to not confuse RPMs for different distributions
execute_process(COMMAND lsb_release -is OUTPUT_VARIABLE lsb_release_id OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND lsb_release -rs OUTPUT_VARIABLE lsb_release_release OUTPUT_STRIP_TRAILING_WHITESPACE)

# Check if the distribution is AlmaLinux, RHEL or CentOS
if ((lsb_release_id MATCHES "CentOS") OR (lsb_release_id MATCHES "AlmaLinux") OR (lsb_release_id MATCHES "RedHatEnterprise"))
  if (lsb_release_release MATCHES "^9.")
    message(STATUS "Recognized CentOS/AlmaLinux/RHEL 9.x")
    set(CPACK_SYSTEM_NAME "el9")
  endif ()
  if (lsb_release_release MATCHES "^8.")
    message(STATUS "Recognized CentOS/AlmaLinux/RHEL 8.x")
    set(CPACK_SYSTEM_NAME "centos8")
  endif ()
  if (lsb_release_release MATCHES "^7.")
    message(STATUS "Recognized CentOS/AlmaLinux/RHEL 7.x")
    set(CPACK_SYSTEM_NAME "cc7")
  endif ()
endif ()

# "Automatically" parse git describe into RPM version
execute_process(COMMAND git describe --tags OUTPUT_VARIABLE git_describe_output OUTPUT_STRIP_TRAILING_WHITESPACE)
message("NOTE: git describe returned ${git_describe_output}")
string(REPLACE "." ";" versions_dot_sep "${git_describe_output}")
list(GET versions_dot_sep 0 CPACK_PACKAGE_VERSION_MAJOR)
list(GET versions_dot_sep 1 CPACK_PACKAGE_VERSION_MINOR)
list(GET versions_dot_sep 2 CPACK_PACKAGE_VERSION_PATCH)

# Concatenate version components into CPACK_PACKAGE_VERSION
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")

#set(CPACK_PACKAGE_RELEASE "")

set(CPACK_PACKAGE_NAME "OpcUaAtlasWienerServer" )
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")
set(CPACK_PACKAGE_CONTACT "Paris Moschovakos paris.moschovakos@cern.ch")
set(CPACK_PACKAGE_VENDOR "paris.moschovakos@cern.ch")
set(CPACK_PACKAGING_INSTALL_PREFIX "/opt/${CPACK_PACKAGE_NAME}")
set(CPACK_RPM_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION}")
set(CPACK_RPM_PACKAGE_NAME "${CPACK_PACKAGE_NAME}")
set(CPACK_RPM_PACKAGE_LICENSE "MIT")
set(CPACK_RPM_PACKAGE_GROUP "CERN")
set(CPACK_RPM_PACKAGE_URL "https://quasar.docs.cern.ch")
set(CPACK_GENERATOR "RPM")

# And also to the version indicator
file(WRITE ${PROJECT_SOURCE_DIR}/Server/include/version.h "#define VERSION_STR \"${git_describe_output}\"" )

install(TARGETS OpcUaAtlasWienerServer RUNTIME DESTINATION "${CPACK_PACKAGING_INSTALL_PREFIX}/bin" )
install(
  FILES
    ${PROJECT_SOURCE_DIR}/bin/ServerConfig.xml
    ${PROJECT_SOURCE_DIR}/bin/config.xml
    ${PROJECT_SOURCE_DIR}/bin/server_watchdog.sh
    ${PROJECT_SOURCE_DIR}/bin/standard_crate.xml
    ${PROJECT_SOURCE_DIR}/bin/update_xml_namespace.sh
    ${PROJECT_SOURCE_DIR}/bin/OpcUaAtlasWienerServer.conf 
    ${PROJECT_SOURCE_DIR}/build/CanInterface/libopcua_atlas_wiener_sock_can.so
  DESTINATION "${CPACK_PACKAGING_INSTALL_PREFIX}/bin")

install(
    FILES
    ${PROJECT_SOURCE_DIR}/Documentation/README.html
    ${PROJECT_SOURCE_DIR}/Documentation/AddressSpaceDoc.html
    ${PROJECT_SOURCE_DIR}/Documentation/ConfigDocumentation.html
  DESTINATION "${CPACK_PACKAGING_INSTALL_PREFIX}/doc")

install(FILES ${PROJECT_BINARY_DIR}/Configuration/Configuration.xsd DESTINATION "${CPACK_PACKAGING_INSTALL_PREFIX}/Configuration" )

set(CPACK_RPM_POST_INSTALL_SCRIPT_FILE "${CMAKE_SOURCE_DIR}/bin/post_install.sh")

include(CPack)