/*
 * NonMonitoredItem.h
 *
 *  Created on: Dec 18, 2014
 *      Author: pnikiel
 *
 *  To understand non-monitored items, look at [1], p. 10-14
 *
 *  A non-monitored item:
 *  - corresponds to 1 parameter of the device
 *  - which is read using one or more communication objects
 *  - is not read periodically (a request to update has to be issued)
 *
 *  The non-monitored item is primarily to handle all data that come through IDcfg and IDUcfg subObjects,
 *  for example CAN Crate SW Version, Fan SW Version, Voltage/Current Setpoints etc.
 *
 *
 */

#ifndef DEVICE_INCLUDE_NONMONITOREDITEM_H_
#define DEVICE_INCLUDE_NONMONITOREDITEM_H_

#include <vector>
#include <boost/function.hpp>
#include <initializer_list>

namespace Device
{

// Forward decls
class NonMonitoredItemsController;

class NonMonitoredItem
{
public:
	//! The handler function gets arguments: firstByte, remaining data, stream length
	typedef boost::function<bool (unsigned char, const unsigned char* data, unsigned int len)> HandlerType;
	NonMonitoredItem (
			unsigned char requestSubObjectId,
			unsigned char requestFirstByte,
			unsigned char replySubObjectId,
			unsigned char replyFirstByte,
			unsigned int numChunks,
			HandlerType handler,
			NonMonitoredItemsController *controller
			);

	//! Schedules updating of this object
	void requestUpdate ();

	//! This should be called by particular NonMonitoredObject that belongs to NonMonitoredItem
	bool handleReply ( unsigned char firstByte, const unsigned char* data, unsigned char length );

	bool operator==(const NonMonitoredItem& other);

	unsigned char getRequestSubObjectId() const { return m_requestSubObjectId; }
	unsigned char getRequestFirstByte() const { return m_requestFirstByte; }

	void setExtraRequestData( std::initializer_list<unsigned char> data );
private:
	unsigned char m_requestSubObjectId;
	unsigned char m_requestFirstByte;
	unsigned char m_replySubObjectId;
	unsigned char m_replyFirstByte;
	//! How many pieces the value is composed of
	unsigned char m_numChunks;

	HandlerType m_onUpdate;
	NonMonitoredItemsController *m_controller;

	std::vector<unsigned char> m_extraRequestData;

class Chunk
{
public:
		enum { CHUNK_SIZE = 7 };

		void reset ();
		bool isObtained () const { return m_isObtained; }
		const unsigned char* data() const { return m_data; }
		void setData(const unsigned char* data);
		Chunk(): m_isObtained(false) {}
		unsigned int size() { return sizeof m_data;  };

private:
		bool m_isObtained;
		unsigned char m_data[CHUNK_SIZE]; /* It is always 7 bytes that go into 1 chunk */
};

	std::vector<Chunk> m_chunks;

	void mergeChunks ();


};



}




#endif /* DEVICE_INCLUDE_NONMONITOREDITEM_H_ */
