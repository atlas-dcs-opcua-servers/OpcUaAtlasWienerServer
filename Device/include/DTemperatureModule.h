
/* This is device header stub */


#ifndef __DTemperatureModule__H__
#define __DTemperatureModule__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DTemperatureModule.h>


namespace Device
{




class
    DTemperatureModule
    : public Base_DTemperatureModule
{

public:
    /* sample constructor */
    explicit DTemperatureModule ( const Configuration::TemperatureModule & config

                                  , DCrate * parent

                                ) ;
    /* sample dtr */
    ~DTemperatureModule ();




    /* delegators for
    cachevariables and sourcevariables */


private:

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void handleTemperatureUpdate ( unsigned char numBytes, const unsigned char* data, bool responseObtained );
    void handleStatusUpdate ( unsigned char tempErrorFlags, unsigned char extTempErrorFlags, bool validData );


private:


};





}

#endif // include guard
