
/* This is device header stub */


#ifndef __DFan__H__
#define __DFan__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DFan.h>


namespace Device
{




class
    DFan
    : public Base_DFan
{

public:
    /* sample constructor */
    explicit DFan ( const Configuration::Fan & config

                    , DFanModule * parent

                  ) ;
    /* sample dtr */
    ~DFan ();




    /* delegators for
    cachevariables and sourcevariables */


private:

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void updateSpeed (unsigned char speed);

private:


};





}

#endif // include guard
