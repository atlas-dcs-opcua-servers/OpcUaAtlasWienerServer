
/* This is device header stub */


#ifndef __DCrate__H__
#define __DCrate__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>
#include <CrateCommunicationController.h>

#include <NonMonitoredItem.h>
#include <NonMonitoredItemsController.h>
#include <atomic>


#include <Base_DCrate.h>


namespace Device
{




class
    DCrate
    : public Base_DCrate
{

public:
    /* sample constructor */
    explicit DCrate ( const Configuration::Crate & config

                      , DCanBus * parent

                    ) ;
    /* sample dtr */
    ~DCrate ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeQueryPeriod ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeOnOffCrate ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeVmeSysReset ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeRefreshNonMonitoredItems ( const OpcUa_Int32 & v);


private:
    /* Delete copy constructor and assignment operator */
    DCrate( const DCrate & ) = delete;
    DCrate& operator=(const DCrate &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    /* Part of CAN message router, to route it to/from Communication Controller */
    void handleMessage ( const CanMessage & msg);
    void sendMessage ( const CanMessage & msg, bool rtr=true);


    void handleSubObjectUpdate ( unsigned char subObjectId, unsigned char numBytes, const unsigned char* data, bool hasValidData);
    void handleStatusUpdate (unsigned char numBytes, const unsigned char* data, bool hasValidData);
    void handleVoltageCurrentUpdate ( unsigned int whichPair, unsigned char numBytes, const unsigned char* data, bool hasValidData );


    //! This function should be called regularly in order to maintain proper operation
    void tick ();

    void onConnected ();
    void onDisconnected ();

    void triggerSlowParamsReadout ();


    //! Call it after the server has started, but before actual communication with the hardware begins
    void initialize ();

    // TODO this should be private
    CrateCommunicationController m_communicationController;


    bool handleNonMonitoredItemStringReply( unsigned char firstByte, const unsigned char* data, unsigned int len);

    // operating times come in minutes
    bool handleNonMonitoredItemMinutesReply( unsigned char firstByte, const unsigned char* data, unsigned int len);


    NonMonitoredItemsController *getNonMonitoredItemsController() { return &m_nonMonitoredItemsController; }

    void addNonMonitoredItem( const NonMonitoredItem& item) { m_nonMonitoredItemsCrate.push_back( item ); }
    NonMonitoredItem * findNonMonitoredItem( unsigned char requestSubObjectId, unsigned char requestFirstByte );

    // TODO this possibly should be avoided and appropriate function returning String address of address space part
    std::string getName() const { return m_name; }

    bool isConnected() const { return m_isConnected; }

    std::string getSoftwareVersion() const { return m_softwareVersionCrate; }

private:

    std::vector<NonMonitoredItem> m_nonMonitoredItemsCrate;
    NonMonitoredItemsController m_nonMonitoredItemsController;
    //! To synchronize the object among server main loop and hardware component loop
    boost::mutex m_accessLock;
    std::string m_name;

    timeval m_lastUpTimeRequest;

    //  safe to access by many threads without explicit locking
    std::atomic_bool m_isConnected;

    //! Stores last known status (through IDstat message) - for debugging mostly.
    std::vector<unsigned char> m_lastStatus;

    //! Corresponds to status byte0 bit3 -> true if crate is in any error. Refreshed with every reception of status msg
    bool m_anyError;

    //! I.e. CAN1.00 or CAN1.05 or so ... important to know in order to be able to parse certain params
    std::string m_softwareVersionCrate;


};





}

#endif // include guard
