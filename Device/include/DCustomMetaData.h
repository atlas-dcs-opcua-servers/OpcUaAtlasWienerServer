
/* This is device header stub */


#ifndef __DCustomMetaData__H__
#define __DCustomMetaData__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DCustomMetaData.h>


namespace Device
{




class
    DCustomMetaData
    : public Base_DCustomMetaData
{

public:
    /* sample constructor */
    explicit DCustomMetaData ( const Configuration::CustomMetaData & config

                               , DRoot * parent

                             ) ;
    /* sample dtr */
    ~DCustomMetaData ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DCustomMetaData( const DCustomMetaData & ) = delete;
    DCustomMetaData& operator=(const DCustomMetaData &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void update();

private:

    timeval m_startTime;

};





}

#endif // include guard
