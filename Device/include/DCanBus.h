
/* This is device header stub */


#ifndef __DCanBus__H__
#define __DCanBus__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>
#include <CanMessage.h>

#include <CCanAccess.h>

#include <Base_DCanBus.h>


namespace Device
{




class
    DCanBus
    : public Base_DCanBus
{

public:
    /* sample constructor */
    explicit DCanBus ( const Configuration::CanBus & config

                       , DRoot * parent

                     ) ;
    /* sample dtr */
    ~DCanBus ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DCanBus( const DCanBus & );
    DCanBus& operator=(const DCanBus &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void openCanBus ();
    void stopBus ();
    // TODO: sendMessage should return some sort of error code
    // TODO: rtr should not be given as a param here
    void sendMessage ( const CanMessage & msg, bool rtr=true);
    void handleMessage (const CanMessage & msg);
    void handleErrorMessage( const int errorCode, const char * desc, timeval & t);
    bool listenOnly () const { return m_listenOnly; }
    bool controlEnabled () const { return m_controlEnabled; }
    std::string getName() const { return m_name; }
    void tickStatistics ();
    void tickNumberConnectedDevices ();

private:
    std::string m_hardwareComponentName;
    std::string m_canPortName;
    std::string m_canPortOptions;
    std::string m_name;
    CCanAccess* m_canBusAccess;
    /* When true, no message will be transmitted by the server to the buses. However parsing of incoming messages will still work */////////
    const bool m_listenOnly;
    /* When false, it is not possible to send any control messages. */
    const bool m_controlEnabled;
    timeval m_lastStatisticsUpdate;

};




}

#endif // include guard
