/*
 * NonMonitoredItemsController.h
 *
 *  Created on: Dec 18, 2014
 *      Author: pnikiel
 *
 *  The non-monitored items controller:
 *  - keeps track of updating number of non-monitored items when requested
 */

#ifndef DEVICE_INCLUDE_NONMONITOREDITEMSCONTROLLER_H_
#define DEVICE_INCLUDE_NONMONITOREDITEMSCONTROLLER_H_

#include <list>
#include <vector>
#include <boost/shared_ptr.hpp>

#include <CanMessage.h>
#include <Utils.h>


namespace Device
{

/* Forward decls */
class DCrate;
class NonMonitoredObject;
class NonMonitoredItem;

class NonMonitoredItemsController
{

public:

	NonMonitoredItemsController( DCrate *crate );

	void addObject( boost::shared_ptr<NonMonitoredObject> object );
	void handleMessage (const CanMessage& msg);

	void tick ();

private:

	//! non-monitored objects will moreless come and go
	std::list<boost::shared_ptr<NonMonitoredObject> > m_objects;

	timeval m_lastRequest;
	timeval m_lastReply;

	DCrate * const m_crate;

	//! Means that a request was sent and a response is awaited
	bool m_awaitingReply;


};


}



#endif /* DEVICE_INCLUDE_NONMONITOREDITEMSCONTROLLER_H_ */
