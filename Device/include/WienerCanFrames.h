/*
 * WienerCanFrames.h
 *
 *  Created on: Nov 24, 2014
 *      Author: pnikiel
 */

#ifndef WIENERCANFRAMES_H_
#define WIENERCANFRAMES_H_

#include <CrateCommunicationController.h>
#include <CanMessage.h>
#include <string>

namespace Device
{

void communicationObjectToCanRequestFrame ( const CrateCommunicationObject & object, CanMessage &msg, unsigned int nodeId );
unsigned int getSubObjectId ( const CanMessage &msg );
unsigned int makeCanId (unsigned int nodeId, unsigned int subObjectId );
std::string commandErrorAsString( int error );

}
#endif /* WIENERCANFRAMES_H_ */
