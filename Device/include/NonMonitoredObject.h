/*
 * SlowCommunicationObject.h
 *
 *  Created on: Dec 16, 2014
 *      Author: pnikiel
 *
 *
 *  Here is the object relation:
 *  1 NonMonitoredItem is associated with 1 or more NonMonitoredObjects
 *
 *  Once a reply to NonMonitoredObject's request is obtained, it is passed for appropriate item for its integration.
 *
 */

#ifndef DEVICE_INCLUDE_SLOWCOMMUNICATIONOBJECT_H_
#define DEVICE_INCLUDE_SLOWCOMMUNICATIONOBJECT_H_

#include <CanMessage.h>
#include <sys/time.h>
#include <string>
#include <vector>

namespace Device
{

class NonMonitoredItem;

enum NonMonitoredObjectHandleStatus{ NOT_FOR_ME, NOT_ACCEPTED_RETRY, ACCEPTED };

class NonMonitoredObject
{
public:
	NonMonitoredObject(
			unsigned char reqSubObjectId,
			unsigned char reqFirstByte,
			unsigned char resSubObjectId,
			unsigned char resFirstByte,
			NonMonitoredItem *item,
			std::vector<unsigned char> extraData = {}
			):
		m_reqSubObjectId (reqSubObjectId),
		m_reqFirstByte (reqFirstByte),
		m_resSubObjectId (resSubObjectId),
		m_resFirstByte (resFirstByte),
		m_replyObtained (false),
		m_item (item),
		m_extraRequestData( extraData )
{

}

	//! True if given message is the response for this object
	bool responseMatch ( const CanMessage &msg ) const;
	//! Should be called by the message router when such reply comes from a crate
	void onResponseObtained () { m_replyObtained = true; }
	bool isResponseObtained () const { return m_replyObtained; }
	void requestToCanFrame (unsigned int nodeId, CanMessage &msg) const;

	//! Returns true when given reply was detected as matching reply
	NonMonitoredObjectHandleStatus handleMessage (const CanMessage& msg);

	/* equality check regarding object's parameters in [1], p.10-14 */
	bool operator==(const NonMonitoredObject& other);

	std::string toString();

private:
	unsigned char m_reqSubObjectId;
	unsigned char m_reqFirstByte;
	unsigned char m_resSubObjectId;
	unsigned char m_resFirstByte;
	timeval m_lastRequestTime;
	bool m_replyObtained;

	//! This NonMonitoredItem may have multiple NonMonitoredObjects
	NonMonitoredItem *m_item;

	std::vector<unsigned char> m_extraRequestData;


};

}


#endif /* DEVICE_INCLUDE_SLOWCOMMUNICATIONOBJECT_H_ */
