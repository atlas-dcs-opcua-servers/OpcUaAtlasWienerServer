/*
 * WienerDefines.h
 *
 *  Created on: Nov 24, 2014
 *      Author: pnikiel
 */

#ifndef WIENERDEFINES_H_
#define WIENERDEFINES_H_

namespace Wiener
{

const int IDstat  = 0;
const int IDctrl  = 0x01;
const int IDvc04  = 0x02;
const int IDvc15  = 0x03;
const int IDvc26  = 0x04;
const int IDvc37  = 0x05;
const int IDfan   = 0x06;
const int IDtemp  = 0x07;
const int IDucfgC = 0x09;
const int IDucfgH = 0x0a;
const int IDcfgC  = 0x0b;
const int IDcfgH  = 0x0c;

const unsigned int SubObjectIdMask = 0x780;
const unsigned int SubObjectIdPosition = 7 ;
const unsigned int NodeIdMask = 0x7f;
const unsigned int NodeIdPosition = 0;

/* These are bit numbers in appropriate status bytes */
const int STATUS_BYTE0_POWER_IS_ON =                 0x00;
const int STATUS_BYTE0_NO_EXTERNAL_POWER_INHIBIT =   0x01;
const int STATUS_BYTE0_AC_IN_LIMIT =                 0x02;
const int STATUS_BYTE0_NO_ERROR =                    0x03;
const int STATUS_BYTE0_FANS_OK =                     0x04;
const int STATUS_BYTE0_TRIP_IF_FANS_BROKEN =         0x05;
const int STATUS_BYTE0_TRIP_IF_ANY_ERROR =           0x06;
const int STATUS_BYTE0_VME_BUS_SIGNAL_SYSFAIL_HIGH = 0x07;

/* These ones are not mentioned in the CAN bus protocol doc, but is mentioned in the server doc */
const int STATUS_BYTE1_LOCAL_CONTROL =               0x01;
const int STATUS_BYTE1_UNCOMPATIBLE =                0x02;
const int STATUS_BYTE1_SOFT_START =                  0x04;
/* Those are mentioned normally in CAN bus protocol doc */
const int STATUS_BYTE1_EEPROM_CHANGED =              0x05;
const int STATUS_BYTE1_EEPROM_ERROR =                0x06;
const int STATUS_BYTE1_WRITE_PROTECT =               0x07;

const int COMMAND_BIT_CRATE_SWITCH =                 0x00;
const int COMMAND_BIT_CRATE_ON =                     0x01;
const int COMMAND_BIT_VME_SYSRESET =                 0x02;
const int COMMAND_BIT_ERROR_TRIPOFF_DISABLE =        0x06;
const int COMMAND_BIT_CHANGE_FAN_SPEED =             0x07;

};

#endif /* WIENERDEFINES_H_ */
