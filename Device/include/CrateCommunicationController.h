/*
 * CrateCommunicationController.h
 *
 *  Created on: Nov 24, 2014
 *      Author: pnikiel
 */

#ifndef CRATECOMMUNICATIONCONTROLLER_H_
#define CRATECOMMUNICATIONCONTROLLER_H_

#include <vector>
#include <list>
#include <sys/time.h>
#include <CanMessage.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

#include <statuscode.h>

#include <NonMonitoredObject.h>

namespace Device
{
class DCanBus;
class DCrate;

class CrateCommunicationObject
{
public: /* no offence, I treat it just as a structure */

	unsigned int m_periodMs;
	timeval m_lastRequestTime;
	timeval m_lastResponseTime;
	unsigned char m_requestedNumBytes;
	CrateCommunicationObject (unsigned char subObjectId, unsigned int periodMs, unsigned char requestedNumBytes);

	unsigned char subObjectId() const { return m_subObjectId; }

	//! This should be called by the controller when a request for this object was just transmitted
	void onSendRequest ();

	//! This should be called by the controller when a reply for this object was just received
	void onReceiveReply ();

	//! Refresh period for this object
	void setPeriod( unsigned int periodMs ) { m_periodMs = periodMs; }
	bool m_haveToNotify;


private:
	unsigned char m_subObjectId; /* not intended to be changed after being created, could be public const but then violates STL assignable */

};



class CrateCommunicationController
{

public:
	CrateCommunicationController (DCrate *crate, bool listenOnly):
		m_crate(crate),
		m_listenOnly(listenOnly),
		m_connected(false),
		m_awaitingReply(false)
	{
		gettimeofday( &m_lastRequestTime, 0);
		gettimeofday( &m_lastReplyTime, 0);
	}

	void addCommunicationObject( const CrateCommunicationObject &o) { m_objects.push_back(o); }
	void handleMessage ( const CanMessage &msg );
	void tick ();

	//! Set refresh period for all objects (in miliseconds)
	void setRefreshPeriod( unsigned int periodMs );


private:

	DCrate * m_crate;
	std::vector<CrateCommunicationObject> m_objects;

	//! True when the server is not allowed to send anything onto the bus
	const bool m_listenOnly;

	//! Current connection state as seen be the server
	bool m_connected;

	//! When we sent most recent request to a crate
	timeval m_lastRequestTime;
	//! When we obtained most recent reply from a crate
	timeval m_lastReplyTime;
    //! True when a request was sent, but an answer hasn't arrived yet
	bool m_awaitingReply;




};



}



#endif /* CRATECOMMUNICATIONCONTROLLER_H_ */
