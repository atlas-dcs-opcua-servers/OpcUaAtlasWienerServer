
/* This is device body stub */


#include <boost/foreach.hpp>
#include <boost/bind/bind.hpp>

#include <Configuration.hxx>

#include <DCrate.h>
#include <ASCrate.h>


#include <DChannel.h>

#include <DFanModule.h>

#include <DTemperatureModule.h>


#include <WienerCanFrames.h>
#include <WienerDefines.h>
#include <WienerServerUtils.h>

#include <iostream>
#include <string.h>
#include <algorithm>

#include <LogIt.h>
#include <LogItComponentIds.h>

#include <DCanBus.h>

using namespace std;
using namespace boost::placeholders;

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DCrate::DCrate (const Configuration::Crate & config

                , DCanBus * parent

               ):
    Base_DCrate( config

                 , parent),


    m_communicationController(this, parent->listenOnly()),
	m_nonMonitoredItemsController(this),
	m_name( config.name() ),
	m_isConnected (false),
	m_anyError (false)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */

	/* Ensure there isn't more than one crate with the id */
	if (parent->getCrateById( id() ))
	{
		throw std::runtime_error ("Duplicate id of the crate. Id="+Utils::toString(id()));
	}

	// TODO this should be made in better way
	unsigned int queryPeriod = config.queryPeriod();
	m_communicationController.addCommunicationObject(CrateCommunicationObject( Wiener::IDstat , queryPeriod, 8));
	m_communicationController.addCommunicationObject(CrateCommunicationObject( Wiener::IDvc04 , queryPeriod, 8));
	m_communicationController.addCommunicationObject(CrateCommunicationObject( Wiener::IDvc15 , queryPeriod, 8));
	m_communicationController.addCommunicationObject(CrateCommunicationObject( Wiener::IDvc26 , queryPeriod, 8));
	m_communicationController.addCommunicationObject(CrateCommunicationObject( Wiener::IDvc37 , queryPeriod, 8));
	m_communicationController.addCommunicationObject(CrateCommunicationObject( Wiener::IDfan  , queryPeriod, 8));
	m_communicationController.addCommunicationObject(CrateCommunicationObject( Wiener::IDtemp , queryPeriod, 8));

	/* Here we add all non-monitored items that have scope of the crate. Refer to [1] p. 10-14 */
	/* CAN Crate Control Software Version */
	m_nonMonitoredItemsCrate.push_back( NonMonitoredItem( Wiener::IDcfgH, 128, Wiener::IDcfgC, 0, 1, boost::bind(&DCrate::handleNonMonitoredItemStringReply, this, _1, _2, _3), &m_nonMonitoredItemsController ) );
	/* Power Supply Software Version */
	m_nonMonitoredItemsCrate.push_back( NonMonitoredItem( Wiener::IDcfgH, 128+3, Wiener::IDcfgC, 3, 2, boost::bind(&DCrate::handleNonMonitoredItemStringReply, this, _1, _2, _3), &m_nonMonitoredItemsController ) );
	// Power Supply ID String
	m_nonMonitoredItemsCrate.push_back( NonMonitoredItem( Wiener::IDcfgH, 128+12, Wiener::IDcfgC, 12, 4, boost::bind(&DCrate::handleNonMonitoredItemStringReply, this, _1, _2, _3), &m_nonMonitoredItemsController ) );

	// Power Supply Operating Time
	m_nonMonitoredItemsCrate.push_back( NonMonitoredItem( Wiener::IDcfgH, 128+6, Wiener::IDcfgC, 6, 1, boost::bind(&DCrate::handleNonMonitoredItemMinutesReply, this, _1, _2, _3), &m_nonMonitoredItemsController ) );

	gettimeofday( &m_lastUpTimeRequest, 0);

}

/* sample dtr */
DCrate::~DCrate ()
{
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DCrate::writeQueryPeriod ( const OpcUa_UInt32 & v)
{
	if (v<1000 || v>60000)
		return OpcUa_BadOutOfRange;
	boost::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
	m_communicationController.setRefreshPeriod( v );
	LOG(Log::INF, LOG_DEVICE) << "New query period for crate " << getFullName() << " is " << v << " ms ";
    return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCrate::writeOnOffCrate ( const OpcUa_Boolean & v)
{
	if (getParent()->controlEnabled())
	{
		CanMessage msg;
		msg.c_id = makeCanId(id(), Wiener::IDctrl);
		msg.c_dlc = 1;
		msg.c_data[0] = (1<<Wiener::COMMAND_BIT_CRATE_SWITCH) | (v==OpcUa_True? (1<<Wiener::COMMAND_BIT_CRATE_ON) : 0 );
		sendMessage(msg, false);
		LOG(Log::INF, LOG_DEVICE) << "Sent ON/OFF command to crate " << getFullName() << " with value=" << (int)v;
		return OpcUa_Good;
	}
	else
	{
		return OpcUa_BadUserAccessDenied;
	}


}

/* Note: never directly call this function. */

UaStatus DCrate::writeVmeSysReset ( const OpcUa_Boolean & v)
{
	if (getParent()->controlEnabled())
	{
		CanMessage msg;
		msg.c_id = makeCanId(id(), Wiener::IDctrl);
		msg.c_dlc = 1;
		msg.c_data[0] = (1<<Wiener::COMMAND_BIT_VME_SYSRESET);
		sendMessage(msg, false);
		LOG(Log::INF, LOG_DEVICE) << "Sent VME SysReset to crate " << getFullName();
		return OpcUa_Good;
	}
	else
	{
		return OpcUa_BadUserAccessDenied;
	}
}

/* Note: never directly call this function. */

UaStatus DCrate::writeRefreshNonMonitoredItems ( const OpcUa_Int32 & v)
{
	boost::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
    triggerSlowParamsReadout();
    return OpcUa_Good;
}





// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333



void DCrate::sendMessage ( const CanMessage & msg, bool rtr)
{
	getParent()->sendMessage(msg, rtr);
}

/*! Main entry point from main server loop thread */
void DCrate::tick ()
{
	boost::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
	m_communicationController.tick ();
	m_nonMonitoredItemsController.tick ();

	// Warning: this is a dirty hack for OPCUA-288 Crate's uptime should be a monitored item and not a nonmonitored item
	// in future this should be improved. Every 1 minute we want to reread this parameter

	timeval now;
	gettimeofday( &now, 0);
	if (subtractTimeval( m_lastUpTimeRequest, now) > 60)
	{
		m_lastUpTimeRequest = now;
		m_nonMonitoredItemsCrate[3].requestUpdate();
		m_nonMonitoredItemsCrate[4].requestUpdate();

		NonMonitoredItem fanOperatingTimeItem ( Wiener::IDcfgH, 128+5, Wiener::IDcfgC, 5, 1, 0, 0);
		auto result = std::find( m_nonMonitoredItemsCrate.begin(), m_nonMonitoredItemsCrate.end(), fanOperatingTimeItem );
		if (result != m_nonMonitoredItemsCrate.end() )
			(*result).requestUpdate();
	}

}

//! Main received messages router for Crate and its children
/*! Main entry point from hardware component thread */
void DCrate::handleMessage ( const CanMessage & msg)
{
	boost::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
	unsigned char subObjectId = getSubObjectId(msg);
	if (subObjectId == Wiener::IDcfgC || subObjectId == Wiener::IDucfgC)
		m_nonMonitoredItemsController.handleMessage (msg);
	else if (subObjectId == Wiener::IDcfgH || subObjectId == Wiener::IDucfgH)
	{
		/* This is an echo of some other host transmitting non-monitored requests, let's ignore it */
	}
	else
		m_communicationController.handleMessage(msg);
}



/**!
 * This is the main entry point for monitored items updates. This function is called by the CommunicationController on two occassions
 * 1) some real monitored item update comes from the crate (then hasValidData = true)
 * 2) timeout occurs on requested monitored item (then hasValidData = false)
 */
void DCrate::handleSubObjectUpdate ( unsigned char subObjectId, unsigned char numBytes, const unsigned char* data, bool hasValidData)
{
	switch (subObjectId)
	{
	case Wiener::IDstat:
		handleStatusUpdate(numBytes, data, hasValidData);
		break;
	case Wiener::IDctrl:
		cout << "is this local echo??" << endl;
		break;
	case Wiener::IDvc04:
		handleVoltageCurrentUpdate (0, numBytes, data, hasValidData);
		break;
	case Wiener::IDvc15:
		handleVoltageCurrentUpdate (1, numBytes, data, hasValidData);
		break;
	case Wiener::IDvc26:
		handleVoltageCurrentUpdate (2, numBytes, data, hasValidData);
		break;
	case Wiener::IDvc37:
		handleVoltageCurrentUpdate (3, numBytes, data, hasValidData);
		break;
	case Wiener::IDfan:
		fanmodule()->handleFanUpdate(numBytes, data, hasValidData);
		break;
	case Wiener::IDtemp:
		temperaturemodule()->handleTemperatureUpdate(numBytes, data, hasValidData);
		break;
	default:
		cout << "subobj not supp: " << subObjectId << endl;

	}
}

void DCrate::handleStatusUpdate( unsigned char numBytes, const unsigned char* data, bool hasValidData )
{
	/* compare with previously known state */
	if ( hasValidData )
	{
		if (numBytes != m_lastStatus.size() or
				not std::equal (m_lastStatus.begin(), m_lastStatus.end(), data )  )
		{
			std::string newStatusBytes ("[");
			for (int i=0; i<numBytes; i++)
				newStatusBytes += toHexString(data[i], 2, '0')+" ";
			newStatusBytes += "] ";
			LOG(Log::INF, LOG_DEVICE) << "Crate " << getFullName() << " status has changed to " << newStatusBytes;

			if (numBytes>0)
				m_anyError = ! (data[0] & 0x08);
			if (m_anyError && numBytes>0)
			{
				std::string flagsDescription ("( ");
				if (! (data[0] & 0x01))
					flagsDescription += "power_is_off ";
				if (! (data[0] & 0x02))
					flagsDescription += "external_power_inhibit ";
				if (! (data[0] & 0x04))
					flagsDescription += "power_fail ";
				if (! (data[0] & 0x10))
					flagsDescription += "fans_are_broken ";
				flagsDescription += ')';
				LOG(Log::ERR, LOG_DEVICE) << "Crate " << getFullName() << " reports being in error. flagsDescription may help, it is " << flagsDescription;
			}
			if ((numBytes>0) && (! (data[0] & 0x80)))
			{
				LOG(Log::INF, LOG_DEVICE) << "Crate " << getFullName() << " reports that VME bus sysfail signal is asserted. ";
			}

			// copy new status
			m_lastStatus.assign( data, data+numBytes );
		}
	}

	if (!getAddressSpaceLink())
		return;

	if (!hasValidData)
	{
		/* Everything should be NULL */
		OpcUa_StatusCode s = OpcUa_BadCommunicationError;
		getAddressSpaceLink()->setNullPowerOn( s );
		getAddressSpaceLink()->setNullNoExtInhibit( s );
		getAddressSpaceLink()->setNullAcInLimit( s );
		getAddressSpaceLink()->setNullNoErrors( s );
		getAddressSpaceLink()->setNullTripIfAnyErrorEnable( s );
		getAddressSpaceLink()->setNullNoVmeSysFail( s );
		getAddressSpaceLink()->setNullLocalControl( s );
		getAddressSpaceLink()->setNullUncompatible( s);
		getAddressSpaceLink()->setNullSoftStart( s);
		getAddressSpaceLink()->setNullEepromChanged( s );
		getAddressSpaceLink()->setNullEepromError( s );
		getAddressSpaceLink()->setNullHwWriteProtect( s);
		getAddressSpaceLink()->setNullCrateStatus( s);
		getAddressSpaceLink()->setNullUnderVoltFlags( s );
		getAddressSpaceLink()->setNullOverVoltFlags( s );
		getAddressSpaceLink()->setNullCurrentFlags( s );
		getAddressSpaceLink()->setNullOverVoltageProtFlags( s );
		getAddressSpaceLink()->setNullInhibit( s );
		getAddressSpaceLink()->setNullUserBusFailure( s );
		fanmodule()->handleStatusUpdate( 0, hasValidData );
		temperaturemodule()->handleStatusUpdate( 0, 0, hasValidData);

	}
	else
	{
		// TODO - take original time for this
		const unsigned char byte0 = data[0];
		getAddressSpaceLink()->setPowerOn( (byte0 & (1<<Wiener::STATUS_BYTE0_POWER_IS_ON ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setNoExtInhibit( (byte0 & (1<<Wiener::STATUS_BYTE0_NO_EXTERNAL_POWER_INHIBIT ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setInhibit( (byte0 & (1<<Wiener::STATUS_BYTE0_NO_EXTERNAL_POWER_INHIBIT ))? OpcUa_False:OpcUa_True, OpcUa_Good );
		getAddressSpaceLink()->setAcInLimit( (byte0 & (1<<Wiener::STATUS_BYTE0_AC_IN_LIMIT ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setNoErrors( (byte0 & (1<<Wiener::STATUS_BYTE0_NO_ERROR ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setTripIfAnyErrorEnable( (byte0 & (1<<Wiener::STATUS_BYTE0_TRIP_IF_ANY_ERROR ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setNoVmeSysFail( (byte0 & (1<<Wiener::STATUS_BYTE0_VME_BUS_SIGNAL_SYSFAIL_HIGH ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setUserBusFailure( (byte0 & (1<<Wiener::STATUS_BYTE0_VME_BUS_SIGNAL_SYSFAIL_HIGH ))? OpcUa_False:OpcUa_True, OpcUa_Good );
		fanmodule()->handleStatusUpdate( byte0, hasValidData );
		if (numBytes<2)
			return;
		const unsigned char byte1 = data[1];
		getAddressSpaceLink()->setLocalControl( (byte1 & (1<<Wiener::STATUS_BYTE1_LOCAL_CONTROL ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setUncompatible( (byte1 & (1<<Wiener::STATUS_BYTE1_UNCOMPATIBLE ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setSoftStart((byte1 & (1<<Wiener::STATUS_BYTE1_SOFT_START ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setEepromChanged( (byte1 & (1<<Wiener::STATUS_BYTE1_EEPROM_CHANGED ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setEepromError( (byte1 & (1<<Wiener::STATUS_BYTE1_EEPROM_ERROR ))? OpcUa_True:OpcUa_False, OpcUa_Good);
		getAddressSpaceLink()->setHwWriteProtect( (byte1 & (1<<Wiener::STATUS_BYTE1_WRITE_PROTECT ))? OpcUa_True:OpcUa_False, OpcUa_Good);

		getAddressSpaceLink()->setCrateStatus( byte0 | ((OpcUa_UInt32)byte1 << 8), OpcUa_Good);

		if (numBytes<3)
			return;
		getAddressSpaceLink()->setUnderVoltFlags( data[2], OpcUa_Good, UaDateTime::now() );

		if (numBytes<4)
			return;
		getAddressSpaceLink()->setOverVoltFlags( data[3], OpcUa_Good, UaDateTime::now() );

		if (numBytes<5)
			return;
		/* data[4] = external temperature error flags */
		/* this is handled within datap[7] */

		if (numBytes<6)
			return;
		getAddressSpaceLink()->setCurrentFlags( data[5], OpcUa_Good);

		if (numBytes<7)
			return;
		getAddressSpaceLink()->setOverVoltageProtFlags( data[6], OpcUa_Good);
		if (numBytes<8)
			return;

		temperaturemodule()->handleStatusUpdate( data[4], data[7], hasValidData);


	}



}

/* The updates come in pairs. channel0-4, channel1-5, and so on */
void DCrate::handleVoltageCurrentUpdate ( unsigned int whichPair, unsigned char numBytes, const unsigned char* data, bool hasValidData )
{
	//cout << __PRETTY_FUNCTION__ << " hasValidData=" << hasValidData << endl;
	if (numBytes < 8)
	{
		hasValidData = false;
		// TODO: print error that something is wrong with received message
	}

	for (int i=0; i<2; i++)
	{
		const unsigned int channelId = whichPair + i*4;
		DChannel *channel = getChannelById(channelId);
		if (channel)
		{
			int rawVoltage=0, rawCurrent=0;
			if (hasValidData)
			{
				rawVoltage = (int16_t) ((data[i*4+1] << 8) | data[i*4]  );
				rawCurrent = (int16_t) ((data[i*4+3] << 8) | data[i*4+2]);
			}
			channel->handleVoltageCurrentUpdate(rawVoltage, rawCurrent, hasValidData, m_anyError);

		}
	}
}

//! Called by the Communication Controller when connection is restored
void DCrate::onConnected ()
{
	if (!m_isConnected)
	{
		/* state changed from disconnected->connected */
		LOG(Log::INF, LOG_DEVICE) << "Crate " << getFullName() << " is now CONNECTED ";
		if (getAddressSpaceLink())
			getAddressSpaceLink()->setConnectionState ("CONNECTED", OpcUa_Good);
	}
	m_isConnected = true;
}


//! Called by the Communication Controller when connection is lost - note - may be called multiple times if crates keeps on not replying
void DCrate::onDisconnected ()
{
	if (m_isConnected) // following only for connected->disconnected, dont react on disconnected->disconnected
	{
		/* connection state changed */
		LOG(Log::ERR, LOG_DEVICE) << "Crate " << getFullName() << " went DISCONNECTED ";
		if (getAddressSpaceLink())
			getAddressSpaceLink()->setConnectionState ("DISCONNECTED", OpcUa_Good);
	}
	m_isConnected = false;
	triggerSlowParamsReadout();
}

//! Force refresh of non-monitored ('static') data
// TODO add synchronization
// anybody calling this function should be already synchronized
//! Caller is responsible for synchronization
void DCrate::triggerSlowParamsReadout ()
{
	/* Request readout of slow parameters */
	/* The data below is based on page 13 of [1] */

	for (auto it = m_nonMonitoredItemsCrate.begin(); it!= m_nonMonitoredItemsCrate.end(); it++ )
	{
		it->requestUpdate();
	}
	BOOST_FOREACH (DChannel *ch, channels())
	{
		ch->triggerSlowParamsReadout();
	}
	m_softwareVersionCrate.clear();
}

void DCrate::initialize ()
{
	onDisconnected();
}



/**! Handles responses to non-monitored data which are formatted as strings */
bool DCrate::handleNonMonitoredItemStringReply( unsigned char firstByte, const unsigned char* data, unsigned int len)
{
	std::string s (reinterpret_cast<const char*>(data), len);
	UaString us( s.c_str() );

	switch (firstByte)
	{
	case 0:
		getAddressSpaceLink()->setSoftwareVersionCrate( us, OpcUa_Good );
		m_softwareVersionCrate = us.toUtf8();
		LOG(Log::INF, LOG_DEVICE) << "Crate " << getFullName() << " software version: " << m_softwareVersionCrate;
		break;
	case 3:
		getAddressSpaceLink()->setSoftwareVersionPs( us, OpcUa_Good );
		break;
	case 12:
		getAddressSpaceLink()->setIdStringPs( us, OpcUa_Good );
		break;
	default:
		std::cout << __PRETTY_FUNCTION__ << " doesnt support replies for " << (unsigned int)firstByte << std::endl;
	}
	return true; // accept the answer - assume we can't check the correctness of the reply
}

bool DCrate::handleNonMonitoredItemMinutesReply( unsigned char firstByte, const unsigned char* data, unsigned int len)
{
        if (getSoftwareVersion()=="")
	  return false; // needs to know version first...
        if (getSoftwareVersion()=="CAN1.00") // TODO: should actually match everything < 1.02
	{
   	  getAddressSpaceLink()->setNullOperatingTimePs (OpcUa_BadNotSupported);
	  return true; // well ... dont ask for this object... it is pointless
        }
	if (len<3)
	{
	        LOG(Log::ERR, LOG_DEVICE) << "Crate " << getFullName() << " sends invalid operating time... this may be due to incompatible crate software version.";
	   
		return true; // just forget
	}
	unsigned int minutes = (unsigned int)data[0] | ((unsigned int)data[1] << 8) | ((unsigned int)data[2] << 16);
	// but the value to expose is in days ...
	OpcUa_Float days = OpcUa_Float(minutes) / (60.0 * 24.0);
	switch (firstByte)
	{
	case 6:
		getAddressSpaceLink()->setOperatingTimePs (days, OpcUa_Good);
		break;
	default:
		std::cout << __PRETTY_FUNCTION__ << " doesnt support replies for " << (unsigned int)firstByte << std::endl;
	}
	return true; // accept the answer - assume we can't check the correctness of the reply
}

NonMonitoredItem * DCrate::findNonMonitoredItem( unsigned char requestSubObjectId, unsigned char requestFirstByte )
{
	auto it = std::find_if( m_nonMonitoredItemsCrate.begin(), m_nonMonitoredItemsCrate.end(), [=](NonMonitoredItem &item) -> bool
			{
				return item.getRequestSubObjectId() == requestSubObjectId && item.getRequestFirstByte() == requestFirstByte;
			}

	);
	if (it == m_nonMonitoredItemsCrate.end())
		return 0;
	else
		return &(*it);
}

}


