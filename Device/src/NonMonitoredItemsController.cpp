/*
 * NonMonitoredItemsController.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: pnikiel
 */

#include <boost/foreach.hpp>
#include <iostream>

#include <DCrate.h>

#include <NonMonitoredItemsController.h>
#include <NonMonitoredObject.h>
#include <WienerServerUtils.h>

namespace Device
{

NonMonitoredItemsController::NonMonitoredItemsController( DCrate *crate ):
	m_crate(crate),
	m_awaitingReply(false)
{
	gettimeofday( &m_lastRequest, 0 );
	gettimeofday( &m_lastReply, 0 );

}


// TODO -- check if the message is of approriate type, that is cfg or ucfg
// TODO synchronization
//! Synchronization: the caller is responsible for synchronizing
void NonMonitoredItemsController::handleMessage (const CanMessage& msg)
{
	for (auto it= m_objects.begin(); it!=m_objects.end(); it++)
	{
		NonMonitoredObjectHandleStatus status = (*it)->handleMessage(msg);
		switch (status)
		{
		case NonMonitoredObjectHandleStatus::ACCEPTED:
		{
			m_objects.erase( it );
			gettimeofday( &m_lastReply, 0 );
			m_awaitingReply = false;
			return;
		}; break;

		case NonMonitoredObjectHandleStatus::NOT_ACCEPTED_RETRY:
		{
			gettimeofday( &m_lastReply, 0 );
			m_awaitingReply = false;
			return;
		}

		case NonMonitoredObjectHandleStatus::NOT_FOR_ME:
		{
			continue;
		}

		}

	}
}




//! Important: this function is responsible for the object being passed, including its memory management
//! The caller should synchronize
void NonMonitoredItemsController::addObject( boost::shared_ptr<NonMonitoredObject> object )
{
	// TODO check whether this object -- or rather its equivalent representation -- is already scheduled
	// TODO do it by nice C++ way

	BOOST_FOREACH( boost::shared_ptr<NonMonitoredObject> &o, m_objects )
	{
		if (*object == *o) // uses comparison operator which we defined
		{
			return; /* this object is already present in the list */
		}

	}
	m_objects.push_back( object );
}


// TODO add additional condition that is, send only when reply to previous was received or timeout obtained
/**!
 * Execution context: called from Server's main loop.
 *
 */
void NonMonitoredItemsController::tick ()
{
	timeval now;
	gettimeofday(&now, 0);
	if (m_objects.size() > 0)
	{
		double tSinceLastSlowRequest = subtractTimeval( m_lastRequest, now );
		double tSinceLastSlowReply = subtractTimeval( m_lastReply, now );
		if (
				(!m_awaitingReply && tSinceLastSlowReply > 0.05) || /* 50ms 'pause' after last conversation, no timeout */
				(tSinceLastSlowRequest > 1.0)) 	/* timeout detected */
		{
			// TODO: the time above shouldnt be hardcoded, or at least a constant?
			CanMessage msg;

			/* Take random object on the waiting list */
			unsigned int objectIndex = rand() % m_objects.size();
			auto it = m_objects.begin();
			for (int i=0; i<objectIndex; i++)
				it++;


			(*it)->requestToCanFrame( m_crate->id(), msg );
			m_crate->sendMessage(msg, false);
			m_lastRequest = now;
			m_awaitingReply = true;



		}
	}
}


}


