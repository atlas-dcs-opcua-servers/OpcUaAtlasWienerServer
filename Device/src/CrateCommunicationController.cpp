/*
 * CrateCommunicationController.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: pnikiel
 */



#include <CrateCommunicationController.h>
#include <iostream>
#include <boost/foreach.hpp>
#include <sys/time.h>
#include <Utils.h>
#include <CanMessage.h>
#include <DCrate.h>
#include <DFan.h>
#include <WienerCanFrames.h>
#include <WienerDefines.h>
#include <WienerServerUtils.h>
#include <string.h>
#include <LogIt.h>
#include <LogItComponentIds.h>

using namespace std;

namespace Device
{

CrateCommunicationObject::CrateCommunicationObject (unsigned char subObjectId, unsigned int periodMs, unsigned char requestedNumBytes):
	m_periodMs (periodMs),
	m_requestedNumBytes (requestedNumBytes),
	m_haveToNotify (false),
	m_subObjectId (subObjectId)
{
	gettimeofday (&m_lastRequestTime, 0);
}

void CrateCommunicationObject::onSendRequest ()
{
	gettimeofday(&m_lastRequestTime, 0);
	m_haveToNotify = true;
}

void CrateCommunicationObject::onReceiveReply ()
{
	gettimeofday(&m_lastResponseTime, 0);
	m_haveToNotify = false;

}

//! Synchronization assumption: the caller is responsible for locking
void CrateCommunicationController::tick ()
{
	bool allowedToTransmit=false;
	timeval now;
	gettimeofday(&now, 0);
	double tSinceLastRequest = subtractTimeval(m_lastRequestTime, now);
	/* Detect timeout */
	if (m_awaitingReply)
	{

		if (tSinceLastRequest > 1.0)
		{ // TODO: timeout's time shouldn't be hardcoded this way
			m_awaitingReply = false;
			m_crate->onDisconnected();
			m_connected=false;
		}
	}

	if (!m_awaitingReply && tSinceLastRequest > 0.1)
		allowedToTransmit = true;



	BOOST_FOREACH( CrateCommunicationObject &o, m_objects )
	{
		double tSinceLastRequest = subtractTimeval(o.m_lastRequestTime, now);
		/* This part of error is to handle no responses for sent requests */
		if (o.m_haveToNotify)
		{
			if (tSinceLastRequest > 0.5)
			{
				o.m_haveToNotify = false;
				m_crate->handleSubObjectUpdate(o.subObjectId(), 0, 0, false);
			}

		}

		/* This part is to send out periodic requests */
		if (allowedToTransmit)
		{
			if (tSinceLastRequest*1000 > o.m_periodMs)
			{
				o.onSendRequest();
				CanMessage msg;
				communicationObjectToCanRequestFrame(o, msg, m_crate->id());
				// TODO what if sendMessage fails?? --
				m_crate->sendMessage(msg);
				allowedToTransmit = false;
				m_lastRequestTime = now;
				m_awaitingReply = true;
			}
		}
	}
}

//! Synchronization: caller
void CrateCommunicationController::handleMessage ( const CanMessage &msg )
{
	// TODO refactor this
	/* Is this a RTR ?? If so, we are hearing a request from another server running on the same bus,
	 * which in principle makes sense only when running in listen-only model
	 */

	if (msg.c_rtr)
	{
		//cout << "RTR" << endl;
		/* mark given request object as being sent */
		const unsigned int subObjId = getSubObjectId(msg);
		BOOST_FOREACH (CrateCommunicationObject &o , m_objects)
		{
			if (subObjId == o.subObjectId())
			{
				o.onSendRequest();
			}
		}
	}
	else
	{
		if (!m_connected)
		{
			/* seems connection is restored */
			m_connected = true;
			m_crate->onConnected();

		}
		// TODO -- synchronized
		/* check if this object is registered in the table */
		const unsigned char subObjectId = getSubObjectId(msg);
		BOOST_FOREACH (CrateCommunicationObject &o , m_objects)
		{
			if (subObjectId == o.subObjectId())
			{
				gettimeofday( & o.m_lastResponseTime, 0);
				o.m_haveToNotify = false;
				m_crate->handleSubObjectUpdate(subObjectId, msg.c_dlc, msg.c_data, true);
				m_awaitingReply = false;
				return;
			}
		}


		/* No object registered -- this event should be monitored */
		LOG(Log::ERR, LOG_DEVICE) << "From crate " << m_crate->getFullName() << " object " << (int)subObjectId << " came without prior request.";
	}


}

//! Synchronization: caller
void CrateCommunicationController::setRefreshPeriod( unsigned int periodMs )
{
	BOOST_FOREACH( CrateCommunicationObject &o, m_objects )
	{
		o.setPeriod( periodMs );
	}
}


}
