/*
 * NonMonitoringItem.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: pnikiel
 */


#include <NonMonitoredItem.h>
#include <NonMonitoredObject.h>
#include <NonMonitoredItemsController.h>
#include <LogIt.h>

namespace Device
{

NonMonitoredItem::NonMonitoredItem (
		unsigned char requestSubObjectId,
		unsigned char requestFirstByte,
		unsigned char replySubObjectId,
		unsigned char replyFirstByte,
		unsigned int numChunks,
		HandlerType handler,
		NonMonitoredItemsController *controller
		):
		m_requestSubObjectId( requestSubObjectId ),
		m_requestFirstByte( requestFirstByte ),
		m_replySubObjectId( replySubObjectId ),
		m_replyFirstByte( replyFirstByte ),
		m_numChunks( numChunks ),
		m_onUpdate( handler ),
		m_controller( controller )
{
	/* Create appropriate nu,mer of chunks */
	// TODO think of sth better than for-loop, like assignment or whatever
	for (unsigned int i=0; i<numChunks; i++)
	{
		m_chunks.push_back (Chunk());
	}

}

bool NonMonitoredItem::operator==(const NonMonitoredItem& other)
{
	// INFO: it is intentional that only following fields are compared, as only them shape what in fact goes through the wire to the crate.
	// i.e. m_controller is completely irrelevant
	return m_requestSubObjectId == other.m_requestSubObjectId &&
			m_requestFirstByte == other.m_requestFirstByte &&
			m_replySubObjectId == other.m_replySubObjectId &&
			m_replyFirstByte == other.m_replyFirstByte &&
			m_numChunks == other.m_numChunks;
}

/**  This function was improved to handle NIM crates as well (OPCUA-534).
 * In NIM crates the exponent is shipped only with some objects from Ucfg family, whereas in VME crates it is shipped in all of them.
 */
bool NonMonitoredItem::handleReply ( unsigned char firstByte, const unsigned char* data, unsigned char length )
{
	if (firstByte < m_replyFirstByte)
	{
		LOG(Log::ERR) << "Obtained a reply with firstByte lower than expected, perhaps wrong handler params?";
		return false; // do not accept this message
	}
	unsigned char chunkNumber = firstByte - m_replyFirstByte;
	if (chunkNumber > m_numChunks)
	{
		LOG(Log::ERR) << "Obtained more chunks than expected, wrong handler params?";
		return false; // do not accept this message
	}
	if (m_numChunks == 1)
	{
		/* no need to perform merging -- just pass to the handler as certainly this is the complete answer */
		return m_onUpdate( m_replyFirstByte, data, length);
	}
	else
	{
            /* Note: (OPCUA-598): for these answers length should be
		the max (8 bytes in CAN frame, effectively 7 bytes of
		data). If it is less than this is an error. */
		if (length <= 1) // not 2: because length is the effective length, that is frame dlc minus 1
		{
			/* Notify error condition - not necessary to perform full merging. */
			m_onUpdate( m_replyFirstByte, data, length);
			return true; // message accepted
		}
		else
		{
			/* this is partial answer only as there are more than 1 chunk */
			m_chunks[chunkNumber].setData(data);
			// now issue parsing
			this->mergeChunks();
			return true;
		}
	}
}

void NonMonitoredItem::Chunk::setData(const unsigned char* data)
{
	memcpy( m_data, data, sizeof m_data );
	m_isObtained = true;
}

void NonMonitoredItem::mergeChunks ()
{
	unsigned int fullLength=0;
	/* The necessary condition for that is that all chunks obtained data */
	for (auto it=m_chunks.begin(); it!=m_chunks.end(); it++)
	{
		if (!it->isObtained())
			return; /* merge attemt wasnt successful */
		fullLength += it->size();
	}
	/* Okay, we can merge the data */
	unsigned char* data = new unsigned char[fullLength];
	unsigned int pos=0;
	for (auto it=m_chunks.begin(); it!=m_chunks.end(); it++)
	{
		memcpy( &data[pos], it->data(), it->size());
		pos += it->size();
	}
	/* Call handler */
	m_onUpdate( m_replyFirstByte, data, fullLength );
	delete[] data;


}

void NonMonitoredItem::requestUpdate ()
{
	if (m_extraRequestData.size() > 0)
	{
		if (m_numChunks != 1)
		{
			LOG(Log::ERR) << "Unsupported situation: extra request data are supported only for 1 chunk ";
			return;
		}
		m_controller->addObject( boost::shared_ptr<NonMonitoredObject>( new NonMonitoredObject(m_requestSubObjectId, m_requestFirstByte, m_replySubObjectId, m_replyFirstByte, this, m_extraRequestData ))  );


	}
	else
	// TODO generate objects from item and schedule them for sending
	for (int chunk=0; chunk<m_numChunks; chunk++)
	{

		m_controller->addObject( boost::shared_ptr<NonMonitoredObject>( new NonMonitoredObject(m_requestSubObjectId, m_requestFirstByte+chunk, m_replySubObjectId, m_replyFirstByte+chunk, this)) );

	}
}

 void NonMonitoredItem::setExtraRequestData( std::initializer_list<unsigned char> data )
 {
	 m_extraRequestData = data;
 }

}

