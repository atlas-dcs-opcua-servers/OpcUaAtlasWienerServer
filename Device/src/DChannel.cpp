
/* This is device body stub */


#include <boost/foreach.hpp>
#include <boost/bind/bind.hpp>

#include <Configuration.hxx>

#include <DChannel.h>
#include <ASChannel.h>


#include <WienerDefines.h>
#include <WienerCanFrames.h>
#include <iostream>
#include <LogIt.h>
#include <LogItComponentIds.h>
using namespace std;
using namespace boost::placeholders;

#include <DCrate.h>

namespace Device




{
// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DChannel::DChannel (const Configuration::Channel & config

                    , DCrate * parent

                   ):
    Base_DChannel( config

                   , parent),
    m_voltageMultiplier(0),
    m_currentMultiplier(0),
    m_voltageExponentRead(false),
    m_currentExponentRead(false)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
	unsigned int ui = id() * 16; // Wiener convention for addressing channels, look at [1] p 10-11
	NonMonitoredItemsController *controller = getParent()->getNonMonitoredItemsController();
	/* Output Voltage Settings */
	getParent()->addNonMonitoredItem( NonMonitoredItem(Wiener::IDucfgH, 128+ui+0, Wiener::IDucfgC, ui+0, 1, boost::bind(&DChannel::handleNonMonitoredDataVoltage, this, _1, _2, _3), controller ) );
	/* Current Limit Settings */
	getParent()->addNonMonitoredItem( NonMonitoredItem(Wiener::IDucfgH, 128+ui+1, Wiener::IDucfgC, ui+1, 1, boost::bind(&DChannel::handleNonMonitoredDataCurrent, this, _1, _2, _3), controller ) );
	/* Undervoltage Compare Settings */
	getParent()->addNonMonitoredItem( NonMonitoredItem(Wiener::IDucfgH, 128+ui+2, Wiener::IDucfgC, ui+2, 1, boost::bind(&DChannel::handleNonMonitoredDataVoltage, this, _1, _2, _3), controller ) );
	/* Overvoltage Compare Settings */
	getParent()->addNonMonitoredItem( NonMonitoredItem(Wiener::IDucfgH, 128+ui+3, Wiener::IDucfgC, ui+3, 1, boost::bind(&DChannel::handleNonMonitoredDataVoltage, this, _1, _2, _3), controller ) );
    // minimum Current Compare Settings
	getParent()->addNonMonitoredItem( NonMonitoredItem(Wiener::IDucfgH, 128+ui+4, Wiener::IDucfgC, ui+4, 1, boost::bind(&DChannel::handleNonMonitoredDataCurrent, this, _1, _2, _3), controller ) );
	// Overcurrent Compare Settings
	getParent()->addNonMonitoredItem( NonMonitoredItem(Wiener::IDucfgH, 128+ui+5, Wiener::IDucfgC, ui+5, 1, boost::bind(&DChannel::handleNonMonitoredDataCurrent, this, _1, _2, _3), controller ) );
	// Overvoltage Protection
	getParent()->addNonMonitoredItem( NonMonitoredItem(Wiener::IDucfgH, 128+ui+6, Wiener::IDucfgC, ui+6, 1, boost::bind(&DChannel::handleNonMonitoredDataVoltage, this, _1, _2, _3), controller ) );
	//Output Voltage fine adjustment
	// TODO
}

/* sample dtr */
DChannel::~DChannel ()
{
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

    UaStatus DChannel::writeVoltageSetPointWrite ( const OpcUa_Double & v)
    {
        // Piotr Nikiel: by default this is disabled. Recompile with this
        // flag to let users alter voltage setpoints .....
#ifndef WIENER_OPCUA_ENABLE_SETPOINT_CONTROL
        return OpcUa_BadNotSupported;
#endif

	if (!m_voltageExponentRead)
	{
            LOG(Log::ERR, LOG_DEVICE) << getFullName() << " cannot write: exponent has to be obtained first. Please wait or force channel values update.";
            return OpcUa_BadDataUnavailable;
	}

	CanMessage msg;
	msg.c_id = makeCanId(getParent()->id(), Wiener::IDucfgH);
	msg.c_dlc = 3;
	msg.c_data[0] = id() * 16 + 0; // "ui" in documentation
	unsigned int rawValue = v / m_voltageMultiplier;
	if (rawValue > 0xffff)
	{
            LOG(Log::ERR, LOG_DEVICE) << getFullName() << " rawValue bigger than 16-bit uint.:" << rawValue;
            return OpcUa_BadOutOfRange;
	}
	msg.c_data[1] = rawValue & 0xff;
	msg.c_data[2] = (rawValue & 0xff00) >> 8;
	getParent()->sendMessage (msg, false);

	return OpcUa_Good;
    }



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

    void DChannel::handleVoltageCurrentUpdate(int rawVoltage, int rawCurrent, bool responseObtained, bool crateInError  )
    {
	if (!getAddressSpaceLink())
            return;
	if (responseObtained)
	{
            OpcUa_StatusCode status ( OpcUa_Good );
            if (crateInError)
                status = OpcUa_Uncertain;
            if (m_voltageExponentRead)
                getAddressSpaceLink()->setVoltageValue((float)rawVoltage*m_voltageMultiplier, status);
            if (m_currentExponentRead)
                getAddressSpaceLink()->setCurrentValue((float)rawCurrent*m_currentMultiplier, status);
	}
	else
	{
            getAddressSpaceLink()->setNullVoltageValue( OpcUa_BadCommunicationError );
            getAddressSpaceLink()->setNullCurrentValue( OpcUa_BadCommunicationError );

	}

    }



bool DChannel::handleNonMonitoredDataVoltage( unsigned char firstByte, const unsigned char* data, unsigned int len )
{
  if (getParent()->getSoftwareVersion() == "")
	  return false; // has to be known first, because parsing of parameters depend on it
	/* Refer to [1], p.10-11 */
  	if (len == 1) // this means that there was an error in the query
  	{
  		switch( firstByte & 0x0f )/* Cut off ui part */
  		{
  		case 0: /* Output Voltage Setting */
  			getAddressSpaceLink()->setNullVoltageSetPoint( OpcUa_BadNotSupported );
  			LOG(Log::ERR, LOG_DEVICE) << "For channel " << getFullName() << " for OutputVoltageSetting obtained reply: " << commandErrorAsString(data[0]);
  			return true;
  		case 2: /* Undervoltage Compare Settings */
  			getAddressSpaceLink()->setNullUnderVoltCompSetPoint( OpcUa_BadNotSupported );
  			LOG(Log::ERR, LOG_DEVICE) << "For channel " << getFullName() << " for UndervoltageCompareSettings obtained reply: " << commandErrorAsString(data[0]);
  			return true;
  		case 3: /* Overvoltage Compare Settings */
  			getAddressSpaceLink()->setNullOverVoltCompSetPoint( OpcUa_BadNotSupported );
  			LOG(Log::ERR, LOG_DEVICE) << "For channel " << getFullName() << " for OvervoltageCompareSettings obtained reply: " << commandErrorAsString(data[0]);
  			return true;
  		case 6: /* Overvoltage Protection */
  			getAddressSpaceLink()->setNullOverVoltProtection( OpcUa_BadNotSupported );
  			LOG(Log::ERR, LOG_DEVICE) << "For channel " << getFullName() << " for OvervoltageProtection obtained reply: " << commandErrorAsString(data[0]);
  			return true;
  		}
  		LOG(Log::ERR, LOG_DEVICE) << "Value not handled in the case statement: logic error ";
  		return true; // accept the answer, no point in requerying
  	}

	/* For NIM crates the answer might actually be less than 7 bytes long , but then we can accept only if the multiplier is set. */
	if (len<7 && !m_voltageExponentRead )
	{
		return false;
	}

	if (!m_voltageExponentRead) 
	{
	  setVoltageMultiplier(data[6]);
	  /* For certain crates, i.e. NIM with software 1.00, the voltage exponent has to be used for current exponent, as the current exponent is not shipped ... ;-( */
          if (getParent()->getSoftwareVersion() == "CAN1.00")
            setCurrentMultiplier(data[6]);

	}

	float voltage = (float)(int16_t)(data[0] | (data[1]<<8)) * m_voltageMultiplier;
	unsigned char paramNo = firstByte & 0x0f; /* Cut off ui part */
	switch( paramNo )
	{
	case 0: /* Output Voltage Setting */
		getAddressSpaceLink()->setVoltageSetPoint( voltage, OpcUa_Good );
		break;
	case 2: /* Undervoltage Compare Settings */
		getAddressSpaceLink()->setUnderVoltCompSetPoint( voltage, OpcUa_Good );
		break;
	case 3: /* Overvoltage Compare Settings */
		getAddressSpaceLink()->setOverVoltCompSetPoint( voltage, OpcUa_Good );
		break;
	case 6: /* Overvoltage Protection */
		getAddressSpaceLink()->setOverVoltProtection( voltage, OpcUa_Good );
		break;
	default:
		throw std::runtime_error("logic_error");

	}
	return true;

}



bool DChannel::handleNonMonitoredDataCurrent( unsigned char firstByte, const unsigned char* data, unsigned int len )
{
  	if (len == 1) // this means that there was an error in the query
  	{
  		switch( firstByte & 0x0f )/* Cut off ui part */
  		{
  		case 1:
  			getAddressSpaceLink()->setNullCurrentLimitSetPoint( OpcUa_BadNotSupported );
  			LOG(Log::ERR, LOG_DEVICE) << "For channel " << getFullName() << " for CurrentLimitSetPoint obtained reply: " << commandErrorAsString(data[0]);
  			break;
  		case 4:
  			getAddressSpaceLink()->setNullMinCurrentCompSetPoint( OpcUa_BadNotSupported );
  			LOG(Log::ERR, LOG_DEVICE) << "For channel " << getFullName() << " for MinCurrentCompSetPoint obtained reply: " << commandErrorAsString(data[0]);
  			break;
  		case 5:
  			getAddressSpaceLink()->setNullOverCurrentCompSetPoint( OpcUa_BadNotSupported );
  			LOG(Log::ERR, LOG_DEVICE) << "For channel " << getFullName() << " for OverCurrentCompSetPoint obtained reply: " << commandErrorAsString(data[0]);
  			break;
  		default:
  			LOG(Log::ERR, LOG_DEVICE) << "Value not handled in the case statement: logic error ";
  		}
  		return true; // no point in querying again
  	}
	/* Refer to [1], p.10-11 */
	if (len<7 && !m_currentExponentRead )
	{
		return false;
	}
	if (!m_currentExponentRead)
	  setCurrentMultiplier(data[6]);

	/*
	 * Important: contrary to what is written in [1], current actually comes as unsigned
	 */
	float current = (float)(uint16_t)(data[0] | (data[1]<<8)) * m_currentMultiplier;
	unsigned char paramNo = firstByte & 0x0f; /* Cut off ui part */
	switch( paramNo )
	{
	case 1:
		getAddressSpaceLink()->setCurrentLimitSetPoint( current, OpcUa_Good );
		break;
	case 4:
		getAddressSpaceLink()->setMinCurrentCompSetPoint( current, OpcUa_Good );
		break;
	case 5:
		getAddressSpaceLink()->setOverCurrentCompSetPoint( current, OpcUa_Good );
		break;
	default:
		throw std::runtime_error("logic_error");

	}
	return true;

}

void DChannel::setVoltageMultiplier( unsigned char exponent )
{
  LOG(Log::DBG, LOG_DEVICE) << "The voltage exponent for " << getFullName() << " is initialized as " << (int)(char)exponent;
	m_voltageMultiplier = pow(10.0, (char)exponent);
	m_voltageExponentRead = true;
}

void DChannel::setCurrentMultiplier( unsigned char exponent )
{
  LOG(Log::DBG, LOG_DEVICE) << "The current exponent for " << getFullName() << " is initialized as " << (int)(char)exponent;
	m_currentMultiplier = pow(10.0, (char)exponent);
	m_currentExponentRead = true;
}

void DChannel::triggerSlowParamsReadout ()
{
	m_voltageExponentRead = false;
	m_currentExponentRead = false;

}


}


