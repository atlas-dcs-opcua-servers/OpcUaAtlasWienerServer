/*
 * WienerCanFrames.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: pnikiel
 */

#include <WienerCanFrames.h>
#include <WienerDefines.h>
#include <Utils.h>

namespace Device
{

void communicationObjectToCanRequestFrame ( const CrateCommunicationObject & object, CanMessage &msg, unsigned int nodeId )
{
	// TODO rework with  define from Wiener
	msg.c_id = ((unsigned int)object.subObjectId() << Wiener::SubObjectIdPosition) | (nodeId & 0x7f);
	msg.c_dlc = object.m_requestedNumBytes;

}

unsigned int getSubObjectId ( const CanMessage &msg )
{
	return (msg.c_id & Wiener::SubObjectIdMask) >> Wiener::SubObjectIdPosition;
}

unsigned int makeCanId (unsigned int nodeId, unsigned int subObjectId )
{
	return ((nodeId << Wiener::NodeIdPosition) & Wiener::NodeIdMask) |
		   ((subObjectId << Wiener::SubObjectIdPosition) & Wiener::SubObjectIdMask);
}

std::string commandErrorAsString( int error )
{
	/* this follows table in [1] p. 12 */
	switch (error)
	{
	case 0: return "OK";
	case 1: return "trying to program 'write protected' data without permission";
	case 2: return "not allowed value (min. value > max. value, min. value > value, max. value < value)";
	case 3: return "undefined command";
	case 4: return "command is not supported by the existing hardware";
	case 252: return "not allowed byte count";
	case 253: return "data overrun";
	case 254: return "hardware error (eeprom checksum not ok)";
	case 255: return "hardware error (unable to access the eeprom data)";
	default: return "error code is unknown (error code="+Utils::toString(error)+")";

	}
}

}
