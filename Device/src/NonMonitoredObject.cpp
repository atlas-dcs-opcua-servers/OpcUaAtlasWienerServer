/*
 * SlowCommunicationObject.cpp
 *
 *  Created on: Dec 16, 2014
 *      Author: pnikiel
 */




#include <NonMonitoredObject.h>
#include <NonMonitoredItem.h>
#include <WienerCanFrames.h>
#include <CanMessage.h>
#include <Utils.h>

namespace Device

{

bool NonMonitoredObject::responseMatch ( const CanMessage &msg ) const
{
	return getSubObjectId(msg)==m_resSubObjectId && msg.c_data[0]==m_resFirstByte ;
}

void NonMonitoredObject::requestToCanFrame (unsigned int nodeId, CanMessage &msg) const
{
	msg.c_id = makeCanId(nodeId, m_reqSubObjectId);
	msg.c_dlc = 1 + m_extraRequestData.size();
	msg.c_data[0] = m_reqFirstByte;
	msg.c_rtr = 0;
	if (m_extraRequestData.size()>7)
		throw std::logic_error("Can't pass extra data bigger than 7");
	for (int i=0; i<m_extraRequestData.size(); i++)
	{
		msg.c_data[1+i] = m_extraRequestData[i];
	}
}

NonMonitoredObjectHandleStatus NonMonitoredObject::handleMessage (const CanMessage& msg)
{
	// TODO check length of the response
	unsigned char subObjId = getSubObjectId( msg );
	if (subObjId != m_resSubObjectId)
		return NonMonitoredObjectHandleStatus::NOT_FOR_ME;
	if (msg.c_data[0] != m_resFirstByte)
		return NonMonitoredObjectHandleStatus::NOT_FOR_ME;
	return m_item->handleReply(msg.c_data[0], &msg.c_data[1], msg.c_dlc-1)?
			NonMonitoredObjectHandleStatus::ACCEPTED : NonMonitoredObjectHandleStatus::NOT_ACCEPTED_RETRY;
}

bool NonMonitoredObject::operator==(const NonMonitoredObject& other)
{
	return
			other.m_reqSubObjectId == this->m_reqSubObjectId &&
			other.m_reqFirstByte == this->m_reqFirstByte &&
			other.m_resSubObjectId == this->m_resSubObjectId &&
			other.m_resFirstByte == this->m_resFirstByte;

}

std::string NonMonitoredObject::toString()
{
	return "req_id="+Utils::toHexString(m_reqSubObjectId)+" req_fb="+Utils::toHexString(m_reqFirstByte);
}

}
