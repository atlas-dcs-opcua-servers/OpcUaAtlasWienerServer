
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DTemperatureModule.h>
#include <ASTemperatureModule.h>


#include <DTemperatureProbe.h>
#include <DCrate.h>



namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DTemperatureModule::DTemperatureModule (const Configuration::TemperatureModule & config

                                        , DCrate * parent

                                       ):
    Base_DTemperatureModule( config

                             , parent)


/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DTemperatureModule::~DTemperatureModule ()
{
}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DTemperatureModule::handleTemperatureUpdate ( unsigned char numBytes, const unsigned char* data, bool responseObtained )
{
	// TODO put all temperatures to bad when hasValidData incorrect
	/* consecutive data bytes contain consecutive temperature probes' data */
	for (unsigned int i=0; i<numBytes; i++)
	{
		DTemperatureProbe *probe = getTemperatureProbeById(i);
		if (probe)
		{
			probe->handleUpdate(data[i], responseObtained);
		}
	}
}

void DTemperatureModule::handleStatusUpdate ( unsigned char tempErrorFlags, unsigned char extTempErrorFlags, bool validData )
{
	if (!getAddressSpaceLink())
		return;
	getAddressSpaceLink()->setTempErrorFlags(tempErrorFlags, OpcUa_Good, UaDateTime::now() );
	getAddressSpaceLink()->setExtTempErrorFlags(extTempErrorFlags, OpcUa_Good, UaDateTime::now() );
}





}


