
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DCustomMetaData.h>
#include <ASCustomMetaData.h>


#include <Utils.h>
#include <WienerServerUtils.h>

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DCustomMetaData::DCustomMetaData (const Configuration::CustomMetaData & config

                                  , DRoot * parent

                                 ):
    Base_DCustomMetaData( config

                          , parent

                        )
/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
	gettimeofday( &m_startTime, 0 );
}

/* sample dtr */
DCustomMetaData::~DCustomMetaData ()
{
}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DCustomMetaData::update()
{
	timeval now;
	gettimeofday( &now, 0 );
	double tdiff = subtractTimeval( m_startTime, now );
	getAddressSpaceLink()->setUptimeDays (tdiff / (60.0 * 60.0 * 24.0), OpcUa_Good);
}



}


