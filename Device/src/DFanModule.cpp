
/* This is device body stub */


#include <boost/foreach.hpp>
#include <boost/bind/bind.hpp>

#include <Configuration.hxx>

#include <DFanModule.h>
#include <ASFanModule.h>


#include <DFan.h>
#include <DCrate.h>

#include <WienerDefines.h>
#include <WienerCanFrames.h>
#include <WienerServerUtils.h>
#include <Utils.h>

#include <LogIt.h>
#include <LogItComponentIds.h>

using namespace boost::placeholders;

namespace Device
{




    // 1111111111111111111111111111111111111111111111111111111111111111111111111
    // 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
    // 1     Users don't modify this code!!!!                                  1
    // 1     If you modify this code you may start a fire or a flood somewhere,1
    // 1     and some human being may possible cease to exist. You don't want  1
    // 1     to be charged with that!                                          1
    // 1111111111111111111111111111111111111111111111111111111111111111111111111






    // 2222222222222222222222222222222222222222222222222222222222222222222222222
    // 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
    // 2     (code for which only stubs were generated automatically)          2
    // 2     You should add the implementation but dont alter the headers      2
    // 2     (apart from constructor, in which you should complete initializati2
    // 2     on list)                                                          2
    // 2222222222222222222222222222222222222222222222222222222222222222222222222

    /* sample ctr */
    DFanModule::DFanModule (const Configuration::FanModule & config

                            , DCrate * parent

                            ):
        Base_DFanModule( config

                         , parent)

        /* fill up constructor initialization list here */
    {
        /* fill up constructor body here */
	getParent()->addNonMonitoredItem( NonMonitoredItem( Wiener::IDcfgH, 128+1, Wiener::IDcfgC, 1, 2, boost::bind(&DFanModule::handleFanSoftwareVersion, this, _1, _2, _3), getParent()->getNonMonitoredItemsController()));
	getParent()->addNonMonitoredItem( NonMonitoredItem( Wiener::IDcfgH, 128+5, Wiener::IDcfgC, 5, 1, boost::bind(&DFanModule::handleFanOperatingTime, this, _1, _2, _3), getParent()->getNonMonitoredItemsController()));
	getParent()->addNonMonitoredItem( NonMonitoredItem( Wiener::IDcfgH, 128+8, Wiener::IDcfgC, 8, 4, boost::bind(&DFanModule::handleFanIdString, this, _1, _2, _3), getParent()->getNonMonitoredItemsController()));
    }

    /* sample dtr */
    DFanModule::~DFanModule ()
    {
    }

    /* delegators for cachevariables and externalvariables */

    /* Note: never directly call this function. */

    UaStatus DFanModule::writeChangeFansSpeed ( const OpcUa_Int32 & v)
    {
	if (v<0 || v>0xff)
	    return OpcUa_BadOutOfRange;
	CanMessage msg;
	msg.c_id = makeCanId(getParent()->id(), Wiener::IDctrl);
	msg.c_dlc = 2;
	msg.c_data[0] = (1<<Wiener::COMMAND_BIT_CHANGE_FAN_SPEED);
	msg.c_data[1] = v;
	getParent()->sendMessage(msg, false);
	return OpcUa_Good;
    }



    // 3333333333333333333333333333333333333333333333333333333333333333333333333
    // 3     FULLY CUSTOM CODE STARTS HERE                                     3
    // 3     Below you put bodies for custom methods defined for this class.   3
    // 3     You can do whatever you want, but please be decent.               3
    // 3333333333333333333333333333333333333333333333333333333333333333333333333

    void DFanModule::handleFanUpdate ( unsigned char numBytes, const unsigned char* data, bool hasValidData )
    {
	if (!getAddressSpaceLink())
            return;
	if (numBytes < 8)
            hasValidData = false; /* TODO print information about too short frame */
	if (hasValidData)
            {
		getAddressSpaceLink()->setMiddleSpeed(60*(unsigned int)data[0], OpcUa_Good);
		getAddressSpaceLink()->setNominalSpeed(60*(unsigned int)data[1], OpcUa_Good);
		BOOST_FOREACH(DFan* fan, fans())
                    {
			const unsigned int byteNum = fan->id() + 2; /* this data starts with byte 2, refer to Wiener doumentation */
			if (byteNum < numBytes)
                            {
				fan->updateSpeed(data[byteNum]);
                            }
                    }
            }
	else
            {
		/* TODO proper handling of fans */
		getAddressSpaceLink()->setMiddleSpeed(0, OpcUa_BadCommunicationError);
		getAddressSpaceLink()->setNominalSpeed(0, OpcUa_BadCommunicationError);
            }


    }

    void DFanModule::handleStatusUpdate ( unsigned char data, bool validData )
    {
	if (!getAddressSpaceLink())
            return;
	getAddressSpaceLink()->setFansOK( data & (1<<Wiener::STATUS_BYTE0_FANS_OK) ? OpcUa_True:OpcUa_False, OpcUa_Good, UaDateTime::now() );
	getAddressSpaceLink()->setTripFansBrokenEnabled( data & (1<<Wiener::STATUS_BYTE0_TRIP_IF_FANS_BROKEN) ? OpcUa_True:OpcUa_False, OpcUa_Good, UaDateTime::now() );
    }

    bool DFanModule::handleFanSoftwareVersion( unsigned char firstByte, const unsigned char* data, unsigned int len)
    {
	if (getAddressSpaceLink())
	{
		if (len<=1)
		{
			// according to [1] error -- first useful byte tells the error code.
			unsigned char errorCode = data[0];
			LOG(Log::ERR, LOG_DEVICE) << "Fan software-version query error for: " << getFullName() << " with error " << int(errorCode) << " : " << commandErrorAsString(errorCode);
			getAddressSpaceLink()->setSoftwareVersionFan( UaString(), OpcUa_BadNoData );
		}
		else
            getAddressSpaceLink()->setSoftwareVersionFan( bytesToUaString(data, len), OpcUa_Good );
	}
	return true;
    }

    bool DFanModule::handleFanOperatingTime( unsigned char firstByte, const unsigned char* data, unsigned int len )
    {
        unsigned int minutes = data[0] | data[1]<<8 | data[2]<<16;
	OpcUa_Float days = OpcUa_Float(minutes) / (60.0 * 24.0);
	if (getAddressSpaceLink())
            getAddressSpaceLink()->setOperatingTimeFan(days, OpcUa_Good);
	return true;
    }

    bool DFanModule::handleFanIdString( unsigned char firstByte, const unsigned char* data, unsigned int len )
    {
		if (getAddressSpaceLink())
		{
			if (len<=1)
			{
				// according to [1] error -- first useful byte tells the error code.
				unsigned char errorCode = data[0];
				LOG(Log::ERR, LOG_DEVICE) << "Fan ID-string query error for: " << getFullName() << " with error " << int(errorCode) << " : " << commandErrorAsString(errorCode);
				getAddressSpaceLink()->setIdStringFan( UaString(), OpcUa_BadNoData );
			}
			else
				getAddressSpaceLink()->setIdStringFan( bytesToUaString(data, len), OpcUa_Good );
		}
	return true;
    }


}


