
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DCanBus.h>
#include <ASCanBus.h>


#include <DCrate.h>

#include <iostream>
using namespace std;

#include <CanBusAccess.h>
#include <LogIt.h>
#include <LogItComponentIds.h>
#include <CanStatistics.h>
#include <WienerServerUtils.h>

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DCanBus::DCanBus (const Configuration::CanBus & config

                  , DRoot * parent

                 ):
    Base_DCanBus( config

                  , parent),
    m_hardwareComponentName(config.type()),
    m_canPortName(config.port()),
    m_canPortOptions(config.speed()),
	m_name(config.name()),
    m_canBusAccess(0),
    m_listenOnly(config.listenOnly()),
	m_controlEnabled(config.controlEnabled())

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
	gettimeofday( &m_lastStatisticsUpdate, 0 );
}

/* sample dtr */
DCanBus::~DCanBus ()
{
}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

    void DCanBus::openCanBus ()
    {
	LOG(Log::INF, LOG_DEVICE) << "CAN bus " << getName() << " requested to be opened.";
	m_canBusAccess = CanBusAccess::getInstance ()->openCanBus(m_hardwareComponentName+":"+m_canPortName,m_canPortOptions);
	if (!m_canBusAccess)
	    throw std::runtime_error("Unable to open CAN bus");
	m_canBusAccess->canMessageCame.connect ( boost::bind( &DCanBus::handleMessage, this, _1 ) );
	m_canBusAccess->canMessageError.connect ( boost::bind( &DCanBus::handleErrorMessage, this, _1, _2, _3 ) );
	m_canBusAccess->updateInitialError();


    }


    void DCanBus::stopBus ()
    {

	if (m_canBusAccess)  // yes, it may be called with NULL m_canBusAccess after an exception thrown while opening the bus
	{
	    LOG(Log::INF, LOG_DEVICE) << "CAN bus " << getName() << " requested to stop.";
	    m_canBusAccess->stopBus();
	}
    }

void DCanBus::handleMessage (const CanMessage & msg)
{
	unsigned int crateId = msg.c_id & 0x7f;
	DCrate *crate = getCrateById(crateId);
	if (crate)
		crate->handleMessage(msg);
	else
	{
		LOG(Log::DBG, LOG_DEVICE) << "CAN bus " << getName() << " : received frame to/from crate which is not present in the config file, id=" << crateId;

	}


}

    void DCanBus::sendMessage ( const CanMessage & msg, bool rtr)
    {
	if (listenOnly())
	{
	    /* NOTHING */
	}
	else
	{
	    unsigned long cobId = msg.c_id;
	    if (cobId > 0xffff)
		cout << __PRETTY_FUNCTION__ << " Big cobid not supported (maybe RTR was intended ? ) " << endl;
	    else
		if (m_canBusAccess)  
		    m_canBusAccess->sendMessage( msg.c_id, msg.c_dlc, (unsigned char*)msg.c_data, rtr );
	}
    }

void DCanBus::tickStatistics ()
{
	timeval tnow;
	gettimeofday( &tnow, 0 );
	float time = subtractTimeval( m_lastStatisticsUpdate, tnow );
	if (time > 1.0)  // TODO: this should be controllable
	{
		m_lastStatisticsUpdate = tnow;
		CanStatistics statistics;
		m_canBusAccess->getStatistics( statistics );
		if (getAddressSpaceLink())
		{
			getAddressSpaceLink()->setStatisticsTotalTx( statistics.totalTransmitted(), OpcUa_Good );
			getAddressSpaceLink()->setStatisticsTotalRx( statistics.totalReceived(), OpcUa_Good );
			getAddressSpaceLink()->setStatisticsTxRate( statistics.txRate(), OpcUa_Good );
			getAddressSpaceLink()->setStatisticsRxRate( statistics.rxRate(), OpcUa_Good );
			getAddressSpaceLink()->setStatisticsBusLoad( /*in percent*/100.0*statistics.busLoad(), OpcUa_Good );

		}
	}
}

void DCanBus::tickNumberConnectedDevices ()
{
	unsigned int connectedDevices=0;
	BOOST_FOREACH( DCrate* crate, crates())
	{
		if (crate->isConnected())
			connectedDevices++;
	}
	getAddressSpaceLink()->setNumberConnectedDevices( connectedDevices, OpcUa_Good );
}

void DCanBus::handleErrorMessage( const int errorCode, const char * desc, timeval & t)
{
	getAddressSpaceLink()->setPortError (errorCode, OpcUa_Good);
	getAddressSpaceLink()->setPortErrorDescription( desc, OpcUa_Good );
}

}


