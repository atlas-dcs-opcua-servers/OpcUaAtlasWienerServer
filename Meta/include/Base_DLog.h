
/* This is device header stub */


#ifndef __Base_DLog__H__
#define __Base_DLog__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DLog.h>

/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASLog
    ;
}
/* forward decl for Parent */

#include <DStandardMetaData.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DGeneralLogLevel;

class DComponentLogLevel;


class
    Base_DLog
{

public:
    /* Constructor */
    explicit Base_DLog ( const Configuration::Log & config

                         , DStandardMetaData * parent

                       ) ;

private:
    /* No copy constructors or assignment operators */
    Base_DLog (const Base_DLog & other) = delete;
    Base_DLog & operator=(const Base_DLog & other) = delete;

public:

    /* sample dtr */
    virtual ~Base_DLog ();

    DStandardMetaData * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */

    void add ( DGeneralLogLevel *);
    const std::vector<DGeneralLogLevel * > & generalloglevels () const {
        return m_GeneralLogLevels;
    }

    void add ( DComponentLogLevel *);
    const std::vector<DComponentLogLevel * > & componentloglevels () const {
        return m_ComponentLogLevels;
    }


    /* to safely quit */
    unsigned int unlinkAllChildren ();


    void linkAddressSpace (AddressSpace::
                           ASLog
                           * as);
    AddressSpace::ASLog * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    /* find methods for children */


    /* getters for values which are keys */



    /* mutex operations */


    /* variable-wise locks */


    /* query address-space for full name (mostly for debug purposes) */
    std::string getFullName() const;

private:


    DStandardMetaData * m_parent;


public:	  // TODO: reconsider this!
    AddressSpace::
    ASLog
    *m_addressSpaceLink;

private:

    std::vector < DGeneralLogLevel * > m_GeneralLogLevels;

    std::vector < DComponentLogLevel * > m_ComponentLogLevels;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* if any of our config entries has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */





};





}

#endif // include guard
