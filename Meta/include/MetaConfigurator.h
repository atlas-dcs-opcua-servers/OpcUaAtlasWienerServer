/*
 * MetaConfigurator.h
 *
 *  Created on: Jun 1, 2015
 *      Author: pnikiel
 */

#ifndef META_INCLUDE_METACONFIGURATOR_H_
#define META_INCLUDE_METACONFIGURATOR_H_



void configureMeta ( Configuration::Configuration & config,
                           AddressSpace::ASNodeManager *nm,
                           UaNodeId parentNodeId  );

void unlinkAllDevicesMeta (AddressSpace::ASNodeManager *nm);

#endif /* META_INCLUDE_METACONFIGURATOR_H_ */
