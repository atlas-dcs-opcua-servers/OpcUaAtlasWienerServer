
/* This is device header stub */


#ifndef __DLog__H__
#define __DLog__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DLog.h>


namespace Device
{




class
    DLog
    : public Base_DLog
{

public:
    /* sample constructor */
    explicit DLog ( const Configuration::Log & config

                    , DStandardMetaData * parent

                  ) ;
    /* sample dtr */
    ~DLog ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DLog( const DLog & ) = delete;
    DLog& operator=(const DLog &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:

private:


};





}

#endif // include guard
