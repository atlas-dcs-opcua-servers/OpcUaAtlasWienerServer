
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>


#include <Base_DLog.h>
#include <ASLog.h>



#include <DGeneralLogLevel.h>

#include <DComponentLogLevel.h>




namespace Device
{


void Base_DLog::linkAddressSpace( AddressSpace::ASLog *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */

void Base_DLog::add ( DGeneralLogLevel *device)
{
    m_GeneralLogLevels.push_back (device);
}

void Base_DLog::add ( DComponentLogLevel *device)
{
    m_ComponentLogLevels.push_back (device);
}


//! Disconnects AddressSpace part from the Device logic, and does the same for all children
//! Returns number of unlinked objects including self
unsigned int Base_DLog::unlinkAllChildren ()
{
    unsigned int objectCounter=1;  // 1 is for self
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */

    BOOST_FOREACH( DGeneralLogLevel *d, m_GeneralLogLevels )
    {
        objectCounter += d->unlinkAllChildren();
    }

    BOOST_FOREACH( DComponentLogLevel *d, m_ComponentLogLevels )
    {
        objectCounter += d->unlinkAllChildren();
    }

    return objectCounter;

}

/* find methods for children */





/* Constructor */
Base_DLog::Base_DLog (const Configuration::Log & config

                      , DStandardMetaData * parent

                     ):

    m_parent(parent),

    m_addressSpaceLink(0)

{

}

Base_DLog::~Base_DLog ()
{
    /* remove children */

    BOOST_FOREACH( DGeneralLogLevel *d, m_GeneralLogLevels )
    {
        delete d;
    }

    BOOST_FOREACH( DComponentLogLevel *d, m_ComponentLogLevels )
    {
        delete d;
    }

}

std::string Base_DLog::getFullName() const
{
    if (m_addressSpaceLink)
    {
        return std::string(m_addressSpaceLink->nodeId().toFullString().toUtf8());
    }
    else
        return "-ObjectNotBoundToAddressSpace-";
}




}


