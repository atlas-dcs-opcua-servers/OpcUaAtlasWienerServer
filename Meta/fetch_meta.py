import os
import shutil
import sys

classes = ['StandardMetaData', 'SVThreadPool', 'Log', 'GeneralLogLevel', 'ComponentLogLevel']

paths=[]

if len(sys.argv) != 2:
    print 'Please supply path to the Meta project, from which files will be copied.'
    sys.exit()

from_prefix = sys.argv[1]+'/'

if not os.path.isdir(from_prefix):
    print 'Given path is not a directory.'
    sys.exit()

for c in classes:
    paths.append({'from': 'AddressSpace/include/AS'+c+'.h', 'to': 'include/AS'+c+'.h'  })
    paths.append({'from': 'AddressSpace/src/AS'+c+'.cpp', 'to': 'src/AS'+c+'.cpp'  })
    paths.append({'from': 'Device/include/D'+c+'.h', 'to': 'include/D'+c+'.h'  })
    paths.append({'from': 'Device/src/D'+c+'.cpp', 'to': 'src/D'+c+'.cpp'  })
    paths.append({'from': 'Device/generated/Base_D'+c+'.h', 'to': 'include/Base_D'+c+'.h'  })
    paths.append({'from': 'Device/generated/Base_D'+c+'.cpp', 'to': 'src/Base_D'+c+'.cpp'  })
    
    
#paths.append({'from': 'Configuration/Configurator.cpp', 'to': 'src/MetaConfigurator.cpp'  })   

    
print paths
for p in paths:
    path_from = from_prefix + p['from']
    path_to = p['to']
    print 'Will copy '+path_from+' to '+path_to
    shutil.copy(path_from, path_to)
