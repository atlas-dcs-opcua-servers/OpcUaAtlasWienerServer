%define name OpcUaAtlasWienerServer 
%define _topdir %(echo $PWD)
%define _tmpdir %{_topdir}/tmp
%define PREFIX /opt/%{name}
%define ld_so_config_path /etc/ld.so.conf.d/%{name}.conf

AutoReqProv: yes 
Summary: %{name}
Name: %{name}
Version: %{version}
Release: %{release}
Source0: checkout.tar.gz
License: zlib/libpng license
Group: Development/Application
BuildRoot: %{_topdir}/BUILDROOT/%{name}-%{version}
BuildArch: x86_64
Prefix: %{PREFIX}
Vendor: CERN 

%description
This is our super-amazing-fancy OPC UA server for Wiener VME Crates. 
Based on Generic OPC UA Server framework by ATLAS Central DCS, CERN.
Responsible: Piotr Nikiel Piotr.Nikiel\@cern.ch

Note that this product was previously called OpcUaWienerServer.

%prep
echo ">>> setup tag" 
echo %{name}

%setup -n checkout 

%build
echo "--- Build ---"
./quasar.py set_build_config build-config-quasar1.6.1-uasdk1.6.5-alma9.cmake || exit 1
./quasar.py build Release --builder Ninja || exit 1
./quasar.py generate as_doc || exit 1
./quasar.py generate config_doc || exit 1

%install
echo "--- Install (don't confuse with installation; nothing is installed on your system now in fact...) ---"
INSTALLED_DIR=%{buildroot}/%{PREFIX}/bin
INSTALLED_DOC_DIR=%{buildroot}/%{PREFIX}/doc
/bin/mkdir -p $INSTALLED_DIR
/bin/mkdir -p $INSTALLED_DOC_DIR
/bin/mkdir -p %{buildroot}/etc/ld.so.conf.d
/bin/cp -v \
    build/bin/OpcUaAtlasWienerServer \
    bin/ServerConfig.xml \
    bin/server_watchdog.sh \
    bin/update_xml_namespace.sh \
    build/Configuration/Configuration.xsd \
    build/CanInterface/libopcua_atlas_wiener_sock_can.so \
    bin/standard_crate.xml \
    $INSTALLED_DIR  # <-- destination

/bin/cp -v Documentation/README.html \
        Documentation/AddressSpaceDoc.html \
	Documentation/ConfigDocumentation.html \
	$INSTALLED_DOC_DIR  # <-- destination

/bin/cp bin/OpcUaAtlasWienerServer.conf                   %{buildroot}/etc/ld.so.conf.d 

%pre
echo "Pre-install: nothing to do"

%post
cd %{PREFIX}/bin
echo "Post-install:"
echo "Setting CAP_NET_ADMIN on Server's binary..."
/usr/sbin/setcap cap_net_admin=ep OpcUaAtlasWienerServer 
echo "Running ldconfig..."
/sbin/ldconfig


echo "Generating OPC UA Server Certificate..."
%{PREFIX}/bin/OpcUaAtlasWienerServer --create_certificate

%preun

if [ $1 = 0 ]; then
	echo "Pre-uninstall: Complete uninstall: will remove files"
fi

%postun
if [ $1 = 0 ]; then
    echo "Post-uninstall: Complete uninstall: will remove files"
    echo "Removing ld.so information..."
	rm -f %{ld_so_config_path}
fi

# Unconditionally run ldconfig in case any .so (shared object) got uninstalled (due to being in %files section)
# This will not hurt and may save some pain in *ss
#
echo "Running ldconfig..."
/sbin/ldconfig


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{PREFIX}
%{ld_so_config_path}

%changelog
