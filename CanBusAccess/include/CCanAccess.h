/*
 * CCanAccess.h
 *
 *  Created on: Apr 4, 2011
 *      Author: vfilimon
 */

#ifndef CCANACCESS_H_
#define CCANACCESS_H_

#include <time.h>
#include "boost/bind/bind.hpp"
#include "boost/signals2.hpp"

#include <CanStatistics.h>

#include <string>

#ifdef WIN32
#include "Winsock2.h"
#endif

#include <CanMessage.h>

using namespace boost::placeholders;

class CCanAccess {
public:
	CCanAccess() = default;
	CCanAccess(const CCanAccess & other) = delete;
	CCanAccess & operator=(CCanAccess const & other) = delete;

	virtual ~CCanAccess() {};

	virtual bool createBUS(const char * ,const char *) = 0 ;
	virtual bool sendRemoteRequest(short ) = 0;
	virtual bool sendMessage(short , unsigned char, unsigned char *, bool rtr) = 0;

	std::string getBusName() { return m_busName; }

	virtual void getStatistics( CanStatistics & result ) = 0;

	virtual bool stopBus () = 0;

	/* TODO: this should be reworked. Since openCanBus opens the bus AND returns the interface (only after that one can connect handlers)
	 * then there is no possibility currently to nicely populate initial error.
	 */
	virtual void updateInitialError () = 0;

	boost::signals2::signal<void (const CanMessage &) > canMessageCame;
	boost::signals2::signal<void (const int,const char *,timeval &) > canMessageError;

protected:
	std::string m_busName;
};

#endif /* CCANACCESS_H_ */
