
#include "CanBusAccess.h"

void CanBusAccess::closeCanBus(CCanAccess *cInter)
{
	ScanManagers.erase(cInter->getBusName().c_str());
	delete cInter;
}

CCanAccess* CanBusAccess::openCanBus(string name,string parameters)
{
	map<string,dlcanbus *>::iterator itcomponent;
    map<string,CCanAccess *>::iterator it;
	dlcanbus *dlcan;

	string nameComponent,nameOfCh;

	it = ScanManagers.find(name);
    if (! ( it == ScanManagers.end() ) ) {
		return (*it).second;
    }

	nameComponent = name.substr(0,name.find_first_of(':'));

	itcomponent = Component.find(nameComponent);

	if (! (itcomponent == Component.end() )) {
		dlcan = (*itcomponent).second;
	}
	else {
		dlcan = new dlcanbus();
		if (dlcan->openInterface((char *)nameComponent.c_str()))
			Component.insert(map<string,dlcanbus *>::value_type(nameComponent,dlcan));
		else return 0;
	}
	            
	CCanAccess *tcca = dlcan->maker_CanAccessObj();
	if (tcca != NULL) {
		const char * pa = parameters.c_str();
		bool c = tcca->createBUS( name.c_str(), pa); 
		if (c) {
			ScanManagers.insert(map<string,CCanAccess *>::value_type(name,tcca));
			return tcca;
		}
	}
    return 0;
}

CanBusAccess* CanBusAccess::m_instance = 0;
