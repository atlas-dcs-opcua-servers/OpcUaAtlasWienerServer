#!/bin/bash
PREFIX="/opt/OpcUaAtlasWienerServer"

echo "Post-install:"

/bin/cp $PREFIX/bin/OpcUaAtlasWienerServer.conf /etc/ld.so.conf.d 

echo "Setting CAP_NET_ADMIN on Server's binary..."
/usr/sbin/setcap cap_net_admin=ep $PREFIX/bin/OpcUaAtlasWienerServer

echo "Running ldconfig..."
/sbin/ldconfig

echo "Generating OPC UA Server Certificate..."
$PREFIX/bin/OpcUaAtlasWienerServer --create_certificate
