#!/bin/bash

SRV=OpcUaWienerServer

source ~/.bashrc
export LD_LIBRARY_PATH=/opt/$SRV/bin:$LD_LIBRARY_PATH
export
echo HOST $HOST
echo HOSTNAME $HOSTNAME
ps efaux | grep -v grep | grep /opt/$SRV/bin/$SRV $1
# if not found - equals to 1, start it
if [ $? -eq 1 ]
then
echo $SRV not running! Restarting...
/opt/$SRV/bin/$SRV $1 >> /localscratch/$SRV.log 2>&1 &
else
echo $SRV running.
fi
echo Done.

