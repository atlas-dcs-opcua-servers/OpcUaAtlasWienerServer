#!/bin/bash

DEST=/localdisk/OpcUaWienerServer
echo "Installing in $DEST"
mkdir  $DEST
echo "Copying needed files"
cd /det/dcs/Development/OpcUaWienerServer/bin
cp -v ServerConfig.xml standard_crate.xml $DEST
cp -v OpcUaWienerServer ../Configuration/Configuration.xsd $DEST 


echo "Now will set sticky (SUID) bit and cap for the server"
sudo setcap cap_net_raw,cap_net_admin,cap_sys_rawio,cap_sys_admin=ep $DEST/OpcUaWienerServer
if [ "$?" != "0" ]; then
	echo "ERROR --- setcap command failed. This must likely mean lack of permissions. Please contact the ATLAS Central DCS team."
	exit 
fi 
echo "Now creating a certificate"
cd $DEST
./OpcUaWienerServer --create_certificate 
cd -

echo "Done"
echo "To run the server:"
echo "cd $DEST"
echo "./OpcUaCanOpenServer <path-to-your-OPCUA-config-file>"


