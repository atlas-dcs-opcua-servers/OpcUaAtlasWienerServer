#!/bin/sh
if [ -z "$1" ]; then
    echo "Please give the path to the configuration XML file as the argument"
    exit
fi
sed -e 's/www.example.org/cern.ch\/quasar/g' $1 -i

