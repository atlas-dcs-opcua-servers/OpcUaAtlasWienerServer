/*
 * CanMessage.h
 *
 *  Created on: Nov 24, 2014
 *      Author: pnikiel
 */

#ifndef CANMESSAGE_H_
#define CANMESSAGE_H_

#include <sys/time.h>

typedef struct CanMsgStruct
{
	long c_id;
	unsigned char c_ff;
	unsigned char c_dlc;
	unsigned char c_data[8];
	timeval	c_time;
	bool c_rtr;
public:
	CanMsgStruct() :
		c_id(0),
		c_ff(0),
		c_dlc(0),
		c_rtr(false)
		{
			// TODO memset
			for (int i=0; i<8; i++)
				c_data[i] = 0;

		}
} CanMessage;


#endif /* CANMESSAGE_H_ */
