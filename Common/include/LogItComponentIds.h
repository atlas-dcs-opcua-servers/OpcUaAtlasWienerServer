/*
 * LogItComponentIds.h
 *
 *  Created on: May 20, 2015
 *      Author: pnikiel
 */

#ifndef COMMON_INCLUDE_LOGITCOMPONENTIDS_H_
#define COMMON_INCLUDE_LOGITCOMPONENTIDS_H_

#include <LogIt.h>

extern Log::LogComponentHandle LOG_DEVICE;
extern Log::LogComponentHandle LOG_CAN_INTERFACE;

#endif /* COMMON_INCLUDE_LOGITCOMPONENTIDS_H_ */
