/*
 * WienerServerUtilities.h
 *
 *  Created on: 6 Nov 2018
 *      Author: pnikiel
 */

#ifndef SERVER_INCLUDE_WIENERSERVERUTILITIES_H_
#define SERVER_INCLUDE_WIENERSERVERUTILITIES_H_

#include <sys/time.h>

#include <uavariant.h>  // not nice to just get uastring.h
#include <string>
#include <sstream>

template<typename T>
static std::string toHexString (const T t, unsigned int width=0, char zeropad=' ')
{
        std::ostringstream oss;
        oss << std::hex;
        if (width > 0)
        {
            oss.width(width);
            oss.fill(zeropad);
        }
        oss << (unsigned long)t << std::dec;
        return oss.str ();
}

double subtractTimeval (const timeval &t1, const timeval &t2);

UaString bytesToUaString( const unsigned char* data, unsigned int len );

std::string errnoToString( );

#endif /* SERVER_INCLUDE_WIENERSERVERUTILITIES_H_ */
