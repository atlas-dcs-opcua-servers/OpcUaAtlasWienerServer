/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QuasarServer.h"
#include <LogIt.h>
#include <LogItComponentIds.h>
#include <string.h>
#include <shutdown.h>
#include <boost/foreach.hpp>
#include <DCanBus.h>
#include <DCrate.h>
#include <DCustomMetaData.h>
#include <DRoot.h>


QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
	usleep(100E3);
	BOOST_FOREACH(Device::DCanBus *canBus, Device::DRoot::getInstance()->canbuss())
	{
	    /* Tick */
	    canBus->tickStatistics();
	    canBus->tickNumberConnectedDevices();
	    BOOST_FOREACH(Device::DCrate *crate, canBus->crates())
	    {
		crate->tick();
	    }
	}
	BOOST_FOREACH(Device::DCustomMetaData *cmd, Device::DRoot::getInstance()->custommetadatas())
	{
	    cmd->update();
	}
    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing OPC UA Wiener VME crates server Device Logic";
    /* Once the system is configured we open CAN buses */
    BOOST_FOREACH( Device::DCanBus* b, Device::DRoot::getInstance()->canbuss() )
    {
	b->openCanBus ();
	/* And initialize all crates on given bus */
	BOOST_FOREACH( Device::DCrate* c, b->crates())
	{
	    c->initialize();
	}
    }


}

void QuasarServer::shutdown()
{
    LOG(Log::INF) << "Shutting down OPC UA Wiener VME crates server Device Logic.";
    BOOST_FOREACH( Device::DCanBus* b, Device::DRoot::getInstance()->canbuss() )
    {
	b->stopBus();
    }


}

void QuasarServer::initializeLogIt()
{
    BaseQuasarServer::initializeLogIt();
    LOG_DEVICE = Log::registerLoggingComponent("Device");
    LOG_CAN_INTERFACE = Log::registerLoggingComponent("CanInterface");
    LOG(Log::INF) << "Logging initialized.";
}
